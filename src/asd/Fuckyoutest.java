/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package asd;


import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.WString;
import com.sun.jna.platform.win32.Kernel32Util;

import domain.libs.sjt.winimpl.window.defs.Kernel32;
import domain.libs.sjt.winimpl.window.defs.MSGByReference;
import domain.libs.sjt.winimpl.window.defs.User32;
import domain.libs.sjt.winimpl.window.defs.User32Direct;
import domain.libs.sjt.winimpl.window.defs.WNDCLASSEX;
import domain.libs.sjt.winimpl.window.defs.WNDPROC;
import domain.libs.sjt.winimpl.wintab.defs.AXIS;
import domain.libs.sjt.winimpl.wintab.defs.LOGCONTEXTW;
import domain.libs.sjt.winimpl.wintab.defs.Wintab32;

public class Fuckyoutest implements WNDPROC {

	public static void main(String[] args) {
		Native.setProtected(true);
		new Fuckyoutest();
	}
	
	
	private Pointer hInstance = Kernel32.INSTANCE.GetModuleHandleW(Pointer.NULL);
	private Pointer hWnd;
	private Pointer hCtx;
	
	
	public Fuckyoutest() {
//		AreaMappingConfig.lol();
//		
		long startTime = System.currentTimeMillis();
		long oldTime = startTime;
		int i=0;
		int x;
		while ((System.currentTimeMillis() - startTime) < 1000*5) {
			double speedMulti = (System.currentTimeMillis() - startTime)/100d;
			x = (int) (i+(100*speedMulti)) % 1280;
			User32Direct.SetCursorPos(x, 100);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(1==1)return;
		int ret;
		WNDCLASSEX wc = new WNDCLASSEX();

		wc.cbSize = wc.size();
		wc.style = 0;
		wc.lpfnWndProc = this;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = hInstance;
		wc.hIcon = User32.INSTANCE.LoadIconW(Pointer.NULL, new Pointer(User32.IDI_APPLICATION));
		wc.hCursor = User32.INSTANCE.LoadCursorW(Pointer.NULL, new Pointer(User32.IDC_ARROW));
		wc.hbrBackground = new Pointer(User32.COLOR_APPWORKSPACE + 1);
		wc.lpszMenuName =  new WString("TiltTestMenu");
		wc.lpszClassName = new WString("TiltTestWClass");
		wc.hIconSm = Pointer.NULL;
		

		ret = User32.INSTANCE.RegisterClassExW(wc);
		if (ret == 0) {
			new Exception("RegisterClassExW() failed. Last error is :" + Kernel32Util.formatMessageFromLastErrorCode(Native.getLastError())).printStackTrace();
			return;
		}
		
		
		if (Wintab32.INSTANCE.WTInfoW(0, 0, Pointer.NULL) == 0) {
			new Exception("WTInfoW() failed").printStackTrace();
			return;
		}
		
		
		hWnd = User32.INSTANCE.CreateWindowExW(
				0,
				new WString("TiltTestWClass"),
				new WString(String.format("TiltTest:%x", Pointer.nativeValue(hInstance))),
				(int) User32.WS_OVERLAPPEDWINDOW,
				(int) User32.CW_USEDEFAULT,
				(int) User32.CW_USEDEFAULT,
				(int) User32.CW_USEDEFAULT,
				(int) User32.CW_USEDEFAULT,
				Pointer.NULL,
				Pointer.NULL,
				hInstance,
				Pointer.NULL);
		
		hCtx = tabletInit();
		if (hCtx == Pointer.NULL) {
			new Exception("tabletInit() failed.").printStackTrace();
			User32.INSTANCE.SendMessageW(hWnd, (int) User32.WM_DESTROY, User32.POINTER_ZERO, User32.POINTER_ZERO);
			return;
		}
		
		if (hWnd == Pointer.NULL) {
			new Exception("CreateWindowExW() failed. Last error is :" + Kernel32Util.formatMessageFromLastErrorCode(Native.getLastError())).printStackTrace();
			return;
		}
		
		User32.INSTANCE.ShowWindow(hWnd, (int) User32.SW_SHOWNORMAL);
		User32.INSTANCE.UpdateWindow(hWnd);
		
		
		MSGByReference msg = new MSGByReference();
		
		while((ret = User32.INSTANCE.GetMessageW(msg, Pointer.NULL, 0, 0 )) != 0) { 
			if (ret == -1) {
				new Exception("GetMessageW() failed. Last error is :" + Kernel32Util.formatMessageFromLastErrorCode(Native.getLastError())).printStackTrace();
				return;
			}
			else {
				User32.INSTANCE.TranslateMessage(msg); 
				User32.INSTANCE.DispatchMessageW(msg); 
			}
		} // while

	}


	@Override
	public Pointer callback(Pointer hWnd, int uMsg, Pointer wParam, Pointer lParam) {
		switch (uMsg) {
		
		
//		case (int) User32.WM_CREATE: // optional
//			break; /* should return zero to continue creation of the window*/
			
			
//		case (int) User32.WM_ACTIVATE:
//			break;
			
			
		case (int) User32.WM_DESTROY:
			tabletCleanup();
			User32.INSTANCE.PostQuitMessage(0);
			
			break; /* If an application processes this message, it should return zero. */
			
			
		case (int) Wintab32.WT_PACKET:
//			System.out.println("WT_PACKET");
			handleWTPACKET(lParam, Pointer.nativeValue(wParam));
			break; /* i assume that default window processing doesn't know (thus care) about WT_PACKET */
		
		
		default:
			return User32.INSTANCE.DefWindowProcW(hWnd, uMsg, wParam, lParam);
			
			
		} // switch
		
		return User32.POINTER_ZERO;
	}


	private Pointer tabletInit() {
		LOGCONTEXTW lc = new LOGCONTEXTW();
		
		Wintab32.INSTANCE.WTInfoW((int) Wintab32.WTI_DDCTXS, 0, lc);

		String lcName = "TiltTest Digitizing %x";
		"TiltTest Digitizing %x".getChars(
				0,
				(int) (lcName.length() > Wintab32.LCNAMELEN ? Wintab32.LCNAMELEN : lcName.length()),
				lc.lcName,
				0);
		
		printLCInfo(lc);
		
		lc.lcOptions |= Wintab32.CXO_MESSAGES;
		lc.lcPktData = (int) (Wintab32.PK_CONTEXT | Wintab32.PK_X | Wintab32.PK_Y);
		lc.lcPktMode = 0; // return all values as absolute (or set tablet to absolute mode?)
		lc.lcMoveMask = Integer.MAX_VALUE;
		lc.lcBtnUpMask = Integer.MAX_VALUE;
		lc.lcBtnDnMask = Integer.MAX_VALUE;
		
//		lc.lcInOrgX = new NativeLong(0);
//		lc.lcInExtX = new NativeLong(30479);
//		
//		lc.lcInOrgY = new NativeLong(0);
//		lc.lcInExtY = new NativeLong(22859);
//
//		lc.lcOutOrgX = new NativeLong(0);
//		lc.lcOutExtX = new NativeLong(30479);
////		
//		lc.lcOutOrgY = new NativeLong(0);
//		lc.lcOutExtY = new NativeLong(22859);
//		
//		lc.lcSysOrgX = 0;
//		lc.lcSysExtX = 100;
//		
//		lc.lcSysOrgY = 0;
//		lc.lcSysExtY = 100;
		
//		lc.lcInExtX = new NativeLong(30479);
		
//		lc.lcOutOrgX = lc.lcInOrgX;
//		lc.lcOutExtX = lc.lcInExtX;
//		
//		lc.lcOutOrgY = lc.lcInOrgY; 
//		lc.lcOutExtY = lc.lcInExtY;
		
//		lc.lcOutOrgX = lc.lcOutOrgY = lc.lcOutExtX = lc.lcOutExtY =
//			lc.lcInOrgX = lc.lcInExtX = lc.lcInOrgY = lc.lcInExtY = new NativeLong(0);
//		
//		lc.lcOutOrgX = lc.lcInOrgX = lc.lcOutOrgY = lc.lcInOrgY =
//			lc.lcOutExtY = lc.lcInExtY = new NativeLong(0);
		
//		Pointer hCtx = Wintab32.INSTANCE.WTOpenW(hWnd, lc, 1);
//		printLCInfo(lc);

		
//		Memory m = new Memory(128);
//
//		for (int i=0;  ; i++) {
//			m.clear();
//			if (Wintab32.INSTANCE.WTInfoW((int) Wintab32.WTI_DSCTXS+i, (int) Wintab32.CTX_INORGX, m) == 0) break;
//			System.out.println("CTX_? for WTI_DSCTXS#"+(i)+": "+m.getNativeLong(0).longValue());
//		}
//		
//		Wintab32.INSTANCE.WTInfoW((int) Wintab32.WTI_INTERFACE, (int) Wintab32.IFC_NDEVICES, m);
//		System.out.println("IFC_NDEVICES: "+m.getNativeLong(0).longValue());
		
		
//		LOGCONTEXTW lcw = new LOGCONTEXTW();
//
//		for (int i=0;  ; i++) {
//			if (Wintab32.INSTANCE.WTInfoW((int) Wintab32.WTI_DSCTXS+i, 0, lcw) == 0) break;
//			System.out.println("");
//			System.out.println("------------------------------------");
//			System.out.println("");
//			System.out.println("LOGCONTEXTW for #"+i);
//			System.out.println("lcName: "+new String(lcw.lcName, 0, getCharArrayStringLength(lcw.lcName)));
//			printLCInfo(lcw);
//		}

//		Memory m = new Memory(128); 
//		for (int i=0;  ; i++) {
//			if (Wintab32.INSTANCE.WTInfoW((int) Wintab32.WTI_EXTENSIONS+i, (int) Wintab32.EXT_TAG, m) == 0) break;
//			if (m.getInt(0) == Wintab32.WTX_CSRMASK) System.out.println("dounf");
//		}
		
		
		Memory m = new Memory(128); 
		for (int i=0;  ; i++) {
			m.clear();
			if (Wintab32.INSTANCE.WTInfoW((int) Wintab32.WTI_EXTENSIONS+i, (int) Wintab32.EXT_NAME, m) == 0) break;
			System.out.println("ext #"+i+" name: "+new String(m.getString(0, true)));
		}
		
//		Memory m = new Memory(Native.POINTER_SIZE);
//		Wintab32.INSTANCE.WTInfoW((int) Wintab32.WTI_DSCTXS, (int) Wintab32.CTX_SYSEXTX, m);
//		System.out.println(m.getNativeLong(0));

		
//		IntByReference i = new IntByReference();
//		int	inOrgX, inExtX, inOrgY, inExtY,
//			outOrgX, outExtX, outOrgY, outExtY,
//			sysOrgX, sysExtX, sysOrgY, sysExtY;
//		
//		int iCat = (int) Wintab32.WTI_DSCTXS;
//		
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_INORGX, i);
//		inOrgX = i.getValue();
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_INEXTX, i);
//		inExtX = i.getValue();
//		
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_INORGY, i);
//		inOrgY = i.getValue();
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_INEXTY, i);
//		inExtY = i.getValue();
//		
//		
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_OUTORGX, i);
//		outOrgX = i.getValue();
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_OUTEXTX, i);
//		outExtX = i.getValue();
//		
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_OUTORGY, i);
//		outOrgY = i.getValue();
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_OUTEXTY, i);
//		outExtY = i.getValue();
//		
//		
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_SYSORGX, i);
//		sysOrgX = i.getValue();
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_SYSEXTX, i);
//		sysExtX = i.getValue();
//		
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_SYSORGY, i);
//		sysOrgY = i.getValue();
//		Wintab32.INSTANCE.WTInfoW(iCat, (int) Wintab32.CTX_SYSEXTY, i);
//		sysExtY = i.getValue();
//		
//		System.out.println(String.format(
//				"		inOrgX = %d   inExtX = %d%n" +
//				"inOrgY = %d%n" +
//				"inExtY = %d%n" +
//				"%n" +
//				"%n" +
//				"		outOrgX = %d   outExtX = %d%n" +
//				"outOrgY = %d%n" +
//				"outExtY = %d%n" +
//				"%n" +
//				"%n" +
//				"		sysOrgX = %d   sysExtX = %d%n" +
//				"sysOrgY = %d%n" +
//				"sysExtY = %d%n",
//				
//				inOrgX, inExtX,
//				inOrgY,
//				inExtY,
//				
//				outOrgX, outExtX,
//				outOrgY,
//				outExtY,
//
//				sysOrgX, sysExtX,
//				sysOrgY,
//				sysExtY
//		));
		
		
		
		Pointer hCtx = Wintab32.INSTANCE.WTOpenW(hWnd, lc, 1);
//		byte csrMask[] = new byte[16];
//		csrMask[0] = (byte)((1<<1)&255);
//		Wintab32.INSTANCE.WTExtGet(hCtx, (int) Wintab32.WTX_CSRMASK, csrMask);
		
//		Wintab32.INSTANCE.WTExtSet(hCtx, (int) Wintab32.WTX_CSRMASK, csrMask);
//		Wintab32.INSTANCE.WTEnable(hCtx, 1);
//		
//		lc.clear();
//		Wintab32.INSTANCE.WTGetW(hCtx, lc);
//		printLCInfo(lc);
		
		
		return hCtx;
	}
	
	
	public static int getCharArrayStringLength(char[] charArray) {
		for (int i=0; i<charArray.length; i++) {
			if (charArray[i] == 0) return i;
		}
		return 0;
	}
	
	
	private void printLCInfo(LOGCONTEXTW lc) {
		AXIS axis = new AXIS();
		
		Wintab32.INSTANCE.WTInfoW((int) Wintab32.WTI_DEVICES, (int) Wintab32.DVC_X, axis);
		System.out.println(String.format(
				"X Axis min = %d   max = %d",
				axis.axMin,
				axis.axMax
				));
		
		Wintab32.INSTANCE.WTInfoW((int) Wintab32.WTI_DEVICES, (int) Wintab32.DVC_Y, axis);
		System.out.println(String.format(
				"Y Axis min = %d   max = %d",
				axis.axMin,
				axis.axMax
				));

		System.out.println(String.format(
				"%n%n=== TABLET'S ACTIVE AREA IN TABLET COORDINATES%n" +
				"					lcInOrgX = %d		lcInExtX = %d%n" +
				"lcInOrgY = %d%n" +
				"lcInExtY = %d%n" +
				"%n" +
				"%n" +
				"=== VALUE REPORTING RANGES%n" +
				"					lcOutOrgX = %d		lcOutExtX = %d%n" +
				"lcOutOrgY = %d%n" +
				"lcOutExtY = %d%n" +
				"%n" +
				"%n" +
				"=== SCREEN MAPPING AREA IN PIXELS (SCREEN COORDINATES)%n" +
				"					lcSysOrgX = %d		lcSysExtX = %d%n" +
				"lcSysOrgY = %d%n" +
				"lcSysExtY = %d",

				lc.lcInOrgX, lc.lcInExtX,
				lc.lcInOrgY, lc.lcInExtY,
				
				lc.lcOutOrgX, lc.lcOutExtX,
				lc.lcOutOrgY, lc.lcOutExtY,
				
				lc.lcSysOrgX, lc.lcSysExtX,
				lc.lcSysOrgY, lc.lcSysExtY
				));
		
	}


	private void tabletCleanup() {
		if (hCtx != Pointer.NULL) {
			Wintab32.INSTANCE.WTClose(hCtx);
		}
	}


//	LOGCONTEXTW lpLogCtx = new LOGCONTEXTW();
//	private Memory m = new Memory(512*1024);
	private void handleWTPACKET(Pointer hCtx, long wSerial) {
//		Wintab32.INSTANCE.WTPacket(hCtx, (int)wSerial, m);
//		Wintab32.INSTANCE.WTGetW(m.getPointer(0), lpLogCtx);
//		System.out.println(lpLogCtx.lcOutOrgX);
//		System.out.println(String.format(
//				"x:%d  y:%d",
//				m.getInt(8),
//				m.getInt(12)
//				));
		
		
	}


}
