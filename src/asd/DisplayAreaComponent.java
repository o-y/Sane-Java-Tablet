/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package asd;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;

public class DisplayAreaComponent extends Component {
	
	private static final long serialVersionUID = 1L;
	private final Rectangle displayBounds;
	private final Rectangle scaledDisplayBounds;
	private final Color clr;

	
	public DisplayAreaComponent(Rectangle displayBounds) {
		this.displayBounds = displayBounds;
		scaledDisplayBounds = new Rectangle(
				(int) (displayBounds.x * DisplayMappingComponent.SCALEVAL),
				(int) (displayBounds.y * DisplayMappingComponent.SCALEVAL),
				(int) (displayBounds.width * DisplayMappingComponent.SCALEVAL),
				(int) (displayBounds.height * DisplayMappingComponent.SCALEVAL));
		
		clr = new Color(
				(int) (Math.random()*254+1),
				(int) (Math.random()*254+1),
				(int) (Math.random()*254+1));
	}
	
	
	public Rectangle getDisplayBounds() {
		return displayBounds;
	}
	
	
	public Rectangle getScaledDisplayBounds() {
		return scaledDisplayBounds;
	}

	
	@Override
	public void paint(Graphics g) {
		g.setColor(clr);
		g.fillRect(0, 0, getWidth(), getHeight());
	}


}
