/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package asd;

import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;

public class AreaMappingConfig extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	
	public static void lol() {
//		try {
//			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//		}
//		catch (Exception e) { }
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new AreaMappingConfig(null, "Fuckyoutest");
			}
		});
	}
	

	// call from AWT thread only!
	public AreaMappingConfig(Window owner, String ownerName) {
		
		super(owner, "Tablet Area Mapping Configuration - " + ownerName);
		
		initComponents();
		
		setVisible(true);
	}
	private void initComponents() {

        configPanelsContainer = new javax.swing.JTabbedPane();
        configRegionsTab = new javax.swing.JPanel();
        tabletPortionContainer = new javax.swing.JPanel();
        tabletSelector = new javax.swing.JComboBox();
        tabletVisualization1 = new javax.swing.JPanel();
        configSetContainer = new javax.swing.JPanel();
        deleteSetButton = new javax.swing.JButton();
        duplicateSetButton = new javax.swing.JButton();
        saveSetButton = new javax.swing.JButton();
        setSelector = new javax.swing.JComboBox();
        mappingsContainer = new javax.swing.JScrollPane();
        mappingsList = new javax.swing.JList();
        saveMappingButton = new javax.swing.JButton();
        destinationPortionContainer = new javax.swing.JPanel();
        dstTabContainer = new javax.swing.JTabbedPane();
        dstTabDisplayContainer = new javax.swing.JPanel();
        displaysVisualization = new javax.swing.JPanel();
        xMinInput = new javax.swing.JSpinner();
        xMaxInput = new javax.swing.JSpinner();
        yMinInput = new javax.swing.JSpinner();
        yMaxInput = new javax.swing.JSpinner();
        dstTabAreaContainer = new javax.swing.JPanel();
        dstAreaInfo = new javax.swing.JLabel();
        dstAreaSelector = new javax.swing.JComboBox();
        configButtonsTab = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        configSetContainer1 = new javax.swing.JPanel();
        deleteSetButton1 = new javax.swing.JButton();
        duplicateSetButton1 = new javax.swing.JButton();
        saveSetButton1 = new javax.swing.JButton();
        setSelector1 = new javax.swing.JComboBox();
        saveMappingButton1 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jComboBox3 = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        configPanelsContainer.setTabPlacement(javax.swing.JTabbedPane.LEFT);

        tabletPortionContainer.setBorder(javax.swing.BorderFactory.createTitledBorder("Map from tablet region"));

        tabletSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "#01 AIRtek tablet", "#02 Wacom tablet", "#03 Generic tablet" }));

        tabletVisualization1.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.light")));

        javax.swing.GroupLayout tabletVisualization1Layout = new javax.swing.GroupLayout(tabletVisualization1);
        tabletVisualization1.setLayout(tabletVisualization1Layout);
        tabletVisualization1Layout.setHorizontalGroup(
            tabletVisualization1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 464, Short.MAX_VALUE)
        );
        tabletVisualization1Layout.setVerticalGroup(
            tabletVisualization1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 208, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout tabletPortionContainer1Layout = new javax.swing.GroupLayout(tabletPortionContainer);
        tabletPortionContainer.setLayout(tabletPortionContainer1Layout);
        tabletPortionContainer1Layout.setHorizontalGroup(
            tabletPortionContainer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabletSelector, 0, 466, Short.MAX_VALUE)
            .addComponent(tabletVisualization1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        tabletPortionContainer1Layout.setVerticalGroup(
            tabletPortionContainer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabletPortionContainer1Layout.createSequentialGroup()
                .addComponent(tabletSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabletVisualization1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        configSetContainer.setBorder(javax.swing.BorderFactory.createTitledBorder("Mapping set"));

        deleteSetButton.setText("Delete set");

        duplicateSetButton.setText("Duplicate set");

        saveSetButton.setText("Save set");

        setSelector.setEditable(true);
        setSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01. Mega-configuration", "02. Precision (work)", "03. Test set" }));

        mappingsList.setModel(new javax.swing.AbstractListModel() {
			private static final long serialVersionUID = 1L;
			String[] strings = { "01. \"one\" to display", "02. \"four\" to specific area", "03. \"three\" to specific area", "04. \"two\" to specific area" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        mappingsContainer.setViewportView(mappingsList);

        saveMappingButton.setText("Save current mapping");

        javax.swing.GroupLayout configSetContainerLayout = new javax.swing.GroupLayout(configSetContainer);
        configSetContainer.setLayout(configSetContainerLayout);
        configSetContainerLayout.setHorizontalGroup(
            configSetContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(deleteSetButton, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
            .addComponent(duplicateSetButton, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
            .addComponent(saveSetButton, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
            .addComponent(setSelector, 0, 212, Short.MAX_VALUE)
            .addComponent(mappingsContainer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
            .addComponent(saveMappingButton, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
        );
        configSetContainerLayout.setVerticalGroup(
            configSetContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(configSetContainerLayout.createSequentialGroup()
                .addComponent(deleteSetButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(duplicateSetButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saveSetButton)
                .addGap(4, 4, 4)
                .addComponent(setSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mappingsContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 473, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saveMappingButton))
        );

        destinationPortionContainer.setBorder(javax.swing.BorderFactory.createTitledBorder("Map to"));

        displaysVisualization.setBorder(new javax.swing.border.LineBorder(javax.swing.UIManager.getDefaults().getColor("Button.light"), 1, true));

        javax.swing.GroupLayout displaysVisualizationLayout = new javax.swing.GroupLayout(displaysVisualization);
        displaysVisualization.setLayout(displaysVisualizationLayout);
        displaysVisualizationLayout.setHorizontalGroup(
            displaysVisualizationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 393, Short.MAX_VALUE)
        );
        displaysVisualizationLayout.setVerticalGroup(
            displaysVisualizationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 288, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout dstTabDisplayContainerLayout = new javax.swing.GroupLayout(dstTabDisplayContainer);
        dstTabDisplayContainer.setLayout(dstTabDisplayContainerLayout);
        dstTabDisplayContainerLayout.setHorizontalGroup(
            dstTabDisplayContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dstTabDisplayContainerLayout.createSequentialGroup()
                .addGroup(dstTabDisplayContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dstTabDisplayContainerLayout.createSequentialGroup()
                        .addComponent(xMinInput, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 295, Short.MAX_VALUE)
                        .addComponent(xMaxInput, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(displaysVisualization, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dstTabDisplayContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(yMinInput, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(yMaxInput, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        dstTabDisplayContainerLayout.setVerticalGroup(
            dstTabDisplayContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dstTabDisplayContainerLayout.createSequentialGroup()
                .addGroup(dstTabDisplayContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(xMinInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xMaxInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dstTabDisplayContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dstTabDisplayContainerLayout.createSequentialGroup()
                        .addComponent(yMinInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 250, Short.MAX_VALUE)
                        .addComponent(yMaxInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(displaysVisualization, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        dstTabContainer.addTab("Display", dstTabDisplayContainer);

        dstAreaInfo.setText("Select an application-specified area to which the mapping will be done to.");

        dstAreaSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tools palette", "Zoomed in window", "Application main window" }));

        javax.swing.GroupLayout dstTabAreaContainerLayout = new javax.swing.GroupLayout(dstTabAreaContainer);
        dstTabAreaContainer.setLayout(dstTabAreaContainerLayout);
        dstTabAreaContainerLayout.setHorizontalGroup(
            dstTabAreaContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dstTabAreaContainerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dstTabAreaContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dstAreaSelector, 0, 441, Short.MAX_VALUE)
                    .addComponent(dstAreaInfo))
                .addContainerGap())
        );
        dstTabAreaContainerLayout.setVerticalGroup(
            dstTabAreaContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dstTabAreaContainerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dstAreaInfo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dstAreaSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(265, Short.MAX_VALUE))
        );

        dstTabContainer.addTab("Specific area", dstTabAreaContainer);

        javax.swing.GroupLayout destinationPortionContainerLayout = new javax.swing.GroupLayout(destinationPortionContainer);
        destinationPortionContainer.setLayout(destinationPortionContainerLayout);
        destinationPortionContainerLayout.setHorizontalGroup(
            destinationPortionContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dstTabContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
        );
        destinationPortionContainerLayout.setVerticalGroup(
            destinationPortionContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dstTabContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout configRegionsTabLayout = new javax.swing.GroupLayout(configRegionsTab);
        configRegionsTab.setLayout(configRegionsTabLayout);
        configRegionsTabLayout.setHorizontalGroup(
            configRegionsTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, configRegionsTabLayout.createSequentialGroup()
                .addGroup(configRegionsTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(destinationPortionContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tabletPortionContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(configSetContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        configRegionsTabLayout.setVerticalGroup(
            configRegionsTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, configRegionsTabLayout.createSequentialGroup()
                .addComponent(tabletPortionContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(destinationPortionContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(configSetContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        configPanelsContainer.addTab("Regions", configRegionsTab);

        jLabel1.setText("Select the tablet whose buttons mappings are to be configured.");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "#01 AIRtek tablet", "#02 Wacom tablet", "#03 Generic tablet" }));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Test buttons here"));

        jLabel2.setFont(jLabel2.getFont().deriveFont(jLabel2.getFont().getSize()+20f));
        jLabel2.setText("1, 5, 0");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                .addContainerGap())
        );

        configSetContainer1.setBorder(javax.swing.BorderFactory.createTitledBorder("Button set"));

        deleteSetButton1.setText("Delete set");

        duplicateSetButton1.setText("Duplicate set");

        saveSetButton1.setText("Save set");

        setSelector1.setEditable(true);
        setSelector1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01. Normal buttons", "02. Weird buttons", "03. Test button set" }));

        saveMappingButton1.setText("Save button mappings");

        javax.swing.GroupLayout configSetContainer1Layout = new javax.swing.GroupLayout(configSetContainer1);
        configSetContainer1.setLayout(configSetContainer1Layout);
        configSetContainer1Layout.setHorizontalGroup(
            configSetContainer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(deleteSetButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
            .addComponent(duplicateSetButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
            .addComponent(saveSetButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
            .addComponent(setSelector1, 0, 212, Short.MAX_VALUE)
            .addComponent(saveMappingButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
        );
        configSetContainer1Layout.setVerticalGroup(
            configSetContainer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(configSetContainer1Layout.createSequentialGroup()
                .addComponent(deleteSetButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(duplicateSetButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saveSetButton1)
                .addGap(4, 4, 4)
                .addComponent(setSelector1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(saveMappingButton1))
        );

        jPanel6.setLayout(new javax.swing.BoxLayout(jPanel6, javax.swing.BoxLayout.LINE_AXIS));

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Configure button #0 as"));

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jComboBox3, 0, 656, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(403, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel7);

        javax.swing.GroupLayout configButtonsTabLayout = new javax.swing.GroupLayout(configButtonsTab);
        configButtonsTab.setLayout(configButtonsTabLayout);
        configButtonsTabLayout.setHorizontalGroup(
            configButtonsTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(configButtonsTabLayout.createSequentialGroup()
                .addGroup(configButtonsTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(configButtonsTabLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(configButtonsTabLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1))
                    .addGroup(configButtonsTabLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jComboBox2, 0, 468, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(configSetContainer1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(configButtonsTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 688, Short.MAX_VALUE)
                .addContainerGap())
        );
        configButtonsTabLayout.setVerticalGroup(
            configButtonsTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(configButtonsTabLayout.createSequentialGroup()
                .addGroup(configButtonsTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(configButtonsTabLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(configSetContainer1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
                .addContainerGap())
        );

        configPanelsContainer.addTab("Buttons", configButtonsTab);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(configPanelsContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 767, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(configPanelsContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
        );

        pack();
    }
	
	
	// Variables declaration - do not modify
    private javax.swing.JPanel configButtonsTab;
    private javax.swing.JTabbedPane configPanelsContainer;
    private javax.swing.JPanel configRegionsTab;
    private javax.swing.JPanel configSetContainer;
    private javax.swing.JPanel configSetContainer1;
    private javax.swing.JButton deleteSetButton;
    private javax.swing.JButton deleteSetButton1;
    private javax.swing.JPanel destinationPortionContainer;
    private javax.swing.JPanel displaysVisualization;
    private javax.swing.JLabel dstAreaInfo;
    private javax.swing.JComboBox dstAreaSelector;
    private javax.swing.JPanel dstTabAreaContainer;
    private javax.swing.JTabbedPane dstTabContainer;
    private javax.swing.JPanel dstTabDisplayContainer;
    private javax.swing.JButton duplicateSetButton;
    private javax.swing.JButton duplicateSetButton1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane mappingsContainer;
    private javax.swing.JList mappingsList;
    private javax.swing.JButton saveMappingButton;
    private javax.swing.JButton saveMappingButton1;
    private javax.swing.JButton saveSetButton;
    private javax.swing.JButton saveSetButton1;
    private javax.swing.JComboBox setSelector;
    private javax.swing.JComboBox setSelector1;
    private javax.swing.JPanel tabletPortionContainer;
    private javax.swing.JComboBox tabletSelector;
    private javax.swing.JPanel tabletVisualization1;
    private javax.swing.JSpinner xMaxInput;
    private javax.swing.JSpinner xMinInput;
    private javax.swing.JSpinner yMaxInput;
    private javax.swing.JSpinner yMinInput;
    // End of variables declaration
	
	
}
