/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package asd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JPanel;

public class DisplayMappingComponent extends Container {

	private static final long serialVersionUID = 1L;
	private final ArrayList<DisplayAreaComponent> displays;
	private Rect2XY displayConfigBounds;
	private Dimension containerSize;
	public final static double SCALEVAL = 0.17;
	private final static int borderWidth = 50;
	
	public DisplayMappingComponent() {
		displays = getDisplayAreas();
		displayConfigBounds = calculateBounds(displays);
		System.out.println(displayConfigBounds);

		displayConfigBounds = new Rect2XY(
				(int) (displayConfigBounds.x1 * SCALEVAL),
				(int) (displayConfigBounds.y1 * SCALEVAL),
				(int) (displayConfigBounds.x2 * SCALEVAL),
				(int) (displayConfigBounds.y2 * SCALEVAL));
		
		containerSize = new Dimension(
				Math.abs(displayConfigBounds.x2 - displayConfigBounds.x1),
				Math.abs(displayConfigBounds.y2 - displayConfigBounds.y1));
		containerSize.width += borderWidth;
		containerSize.height += borderWidth;
		
		setLayout(new BorderLayout());
		
		JPanel j = new JPanel(null);
		j.setBackground(Color.RED);
		
		for (DisplayAreaComponent display : displays) {
			int x = (int) (display.getScaledDisplayBounds().x + Math.abs(displayConfigBounds.x1) + borderWidth/2.0);
			int y = (int) (display.getScaledDisplayBounds().y + Math.abs(displayConfigBounds.y1) + borderWidth/2.0);
			int w = (int) display.getScaledDisplayBounds().width;
			int h = (int) display.getScaledDisplayBounds().height;
			display.setBounds(x,y,w,h);
			j.add(display);
		}
		add(j, BorderLayout.CENTER);

		j.setSize(containerSize);
		j.setPreferredSize(containerSize);
//		setPreferredSize(containerSize);
//		j.setMinimumSize(containerSize);
	}
	
	
	public static ArrayList<DisplayAreaComponent> getDisplayAreas() {
		List<GraphicsDevice> displayDevices = Arrays.asList(GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices());
		ArrayList<DisplayAreaComponent> displayAreas = new ArrayList<DisplayAreaComponent>();
		
		for (GraphicsDevice screen : displayDevices) {
			displayAreas.add(
					new DisplayAreaComponent(screen.getDefaultConfiguration().getBounds()));
					
		}
		
		return displayAreas;
	} // method
	
	
	public static Rect2XY calculateBounds(ArrayList<DisplayAreaComponent> displays) {
		int smallestX = displays.get(0).getDisplayBounds().x;
		int smallestY = displays.get(0).getDisplayBounds().y;
		int biggestX = displays.get(0).getDisplayBounds().x + displays.get(0).getDisplayBounds().width;
		int biggestY = displays.get(0).getDisplayBounds().y + displays.get(0).getDisplayBounds().height;
		
		for (DisplayAreaComponent display : displays) {
			if (display.getDisplayBounds().x < smallestX) smallestX = display.getDisplayBounds().x;
			if (display.getDisplayBounds().y < smallestY) smallestY = display.getDisplayBounds().y;
			if ((display.getDisplayBounds().x + (display.getDisplayBounds().width)) > biggestX) biggestX = display.getDisplayBounds().x + display.getDisplayBounds().width;
			if ((display.getDisplayBounds().y + (display.getDisplayBounds().height)) > biggestY) biggestY = display.getDisplayBounds().y + display.getDisplayBounds().height;
		}

		return new Rect2XY(
				smallestX, smallestY,
				biggestX, biggestY);
	}
	
	
}
