/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.example;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import domain.libs.sjt.SaneJavaTablet;
import domain.libs.sjt.TabletComponentLink;
import domain.libs.sjt.config.TabletConfigurationException;
import domain.libs.sjt.config.TabletConfigurations;
import domain.libs.sjt.entities.TabletDevice;
import domain.libs.sjt.entities.TabletTool;
import domain.libs.sjt.iface.TabletBaseInterface;
import domain.libs.sjt.iface.TabletEventInterface;
import domain.libs.sjt.iface.TabletInterfaceToEvents;
import domain.libs.sjt.impl.TabletImplementationException;
import domain.libs.sjt.misc.ConsoleLogger;


public class SaneJavaTabletExample extends JFrame implements TabletEventInterface {

	private static final long serialVersionUID = 1L;
	private final ConsoleLogger log = ConsoleLogger.newInstanceForCallingClass();

	
	public static void main(String[] args) {
//		if(1==1)return;
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new SaneJavaTabletExample();
			}
		});
	}


	private SaneJavaTablet sjt;
	
	
	public SaneJavaTabletExample() {
		super("SaneJavaTabletExample");
		
		if (initSJT() == false) return;

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(800, 600);
		this.setLocationRelativeTo(null);
		setVisible(true);
		
	} // method


	private boolean initSJT() {
		
		// add Component's to list from who the events are desired
		LinkedList<TabletComponentLink> listenOnComponents = new LinkedList<TabletComponentLink>();
		listenOnComponents.add(new TabletComponentLink(
				this.getContentPane(),
				"lol main window. don't use. end's up sending two messages",
				(TabletBaseInterface) new TabletInterfaceToEvents(this)
				));

		
		// load tablet configuration from a file
		TabletConfigurations tabletConfigs = null;
		try {
			tabletConfigs = TabletConfigurations.loadFrom(new FileInputStream("sjt_example.config"));
		}
		catch (FileNotFoundException e) {
			if(1==2)
			log.error("Failed to load tablet configuration from file '%s'. Reason: '%s'.",
					"xyz.file",
					e.getMessage());
		}
		catch (TabletConfigurationException e) {
			log.error("Failed to load tablet configuration from file '%s'. Reason: '%s'.",
					"xyz.file",
					e.getMessage());
		}
		
		// or create a new one
		if (tabletConfigs == null) {
			tabletConfigs = new TabletConfigurations(listenOnComponents);
		}

		
		// instantiate sjt and make it use the loaded tablet configuration
		// and inform it about what Component's it must listen to
		try {
			sjt = new SaneJavaTablet(true);
			sjt.useSettings(tabletConfigs, listenOnComponents);
		}
		catch (TabletImplementationException e) {
			e.printStackTrace();
			return false;
		}
		catch (TabletConfigurationException e) {
			e.printStackTrace();
			return false;
		}
		
		
		addWindowListener(new WindowAdapter() {
			
			// sjt activates it's own mappings etc. when the window is activated,
			// by the help of this method call.
			// note that if the program consists of many windows,
			// only one call to the method is necessary.
			// consecutive calls to the same method do no harm.
			@Override
			public void windowActivated(WindowEvent e) {
				sjt.enable();
			}
			
			
			// sjt deactivates it's own mappings etc. when the window is deactivated,
			// by the help of this method call.
			// mandatory. sjt cannot be used so it would "replace" the operating
			// system's own tablet mappings, as other tabl�et programs activate their
			// own mappings when they become active.
			// note that if the program consists of many windows,
			// only one call to the method is necessary.
			// consecutive calls to the same method do no harm.
			@Override
			public void windowDeactivated(WindowEvent e) {
				sjt.disable();
			}
			
			
			@Override
			public void windowClosed(WindowEvent e) {
				sjt.close();
			}
		});
		
		return true;
		
	} // method


	@Override
	public void tabletToolEnter(TabletTool tabletTool) {
		System.out.println(String.format(
				"tabletToolEnter() TabletDevice ID:%d, TabletTool ID:%d",
				tabletTool.getTabletDevice().getID(),
				tabletTool.getID()
				));
	}


	@Override
	public void tabletToolExit(TabletTool tabletTool) {
		System.out.println(String.format(
				"tabletToolExit() TabletDevice ID:%d, TabletTool ID:%d",
				tabletTool.getTabletDevice().getID(),
				tabletTool.getID()
				));
	}


	@Override
	public void tabletToolPositionXY(TabletTool tabletTool) {
		System.out.println(String.format(
				"tabletToolPositionXY() TabletDevice ID:%d, TabletTool ID:%d, X:%d, Y:%d",
				tabletTool.getTabletDevice().getID(),
				tabletTool.getID(),
				tabletTool.getX(),
				tabletTool.getY()
				));
	}


	@Override
	public void tabletToolPositionZ(TabletTool tabletTool) {
		System.out.println(String.format(
				"tabletToolPositionZ() TabletDevice ID:%d, TabletTool ID:%d, Z:%d",
				tabletTool.getTabletDevice().getID(),
				tabletTool.getID(),
				tabletTool.getZ()
				));
	}


	@Override
	public void tabletToolTilt(TabletTool tabletTool) {
		System.out.println(String.format(
				"tabletToolTilt() TabletDevice ID:%d, TabletTool ID:%d, Tilt:%s",
				tabletTool.getTabletDevice().getID(),
				tabletTool.getID(),
				tabletTool.getTilt().toString()
				));
	}


	@Override
	public void tabletToolPressure(TabletTool tabletTool, int index) {
		System.out.println(String.format(
				"tabletToolPressure() TabletDevice ID:%d, TabletTool ID:%d, Pressure Index:%d, Value:%f",
				tabletTool.getTabletDevice().getID(),
				tabletTool.getID(),
				index,
				tabletTool.getPressure(index)
				));
	}


	@Override
	public void tabletToolRangedValue(TabletTool tabletTool, int index) {
		System.out.println(String.format(
				"tabletToolRangedValue() TabletDevice ID:%d, TabletTool ID:%d, RangedTool Index:%d, Value:%f",
				tabletTool.getTabletDevice().getID(),
				tabletTool.getID(),
				index,
				tabletTool.getRangedValue(index).getValue()
				));
	}


	@Override
	public void tabletToolButton(TabletTool tabletTool, int index) {
		System.out.println(String.format(
				"tabletToolButton() TabletDevice ID:%d, TabletTool ID:%d, Button Index:%d, Value:%b",
				tabletTool.getTabletDevice().getID(),
				tabletTool.getID(),
				index,
				tabletTool.getButton(index)
				));
	}


	@Override
	public void tabletRangedValue(TabletDevice tabletDevice, int index) {
		System.out.println(String.format(
				"tabletRangedValue() TabletDevice ID:%d, RangedValue Index:%d, Value:%f",
				tabletDevice.getID(),
				index,
				tabletDevice.getRangedValue(index).getValue()
				));
	}


	@Override
	public void tabletButton(TabletDevice tabletDevice, int index) {
		System.out.println(String.format(
				"tabletButton() TabletDevice ID:%d, Button Index:%d, Value:%b",
				tabletDevice.getID(),
				index,
				tabletDevice.getButton(index)
				));
	}




}
