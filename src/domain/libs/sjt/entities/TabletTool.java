/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.entities;


public class TabletTool {
	
	private long id;
	
	private TabletDevice tabletDevice;
	// x, y, proximity if possible, z, ...
	private double positions[];
	// main pressure, secondary pressure, ... 
	private double pressures[];
	// entities that are capable of delivering values in range of 0.0d to 1.0d
	private TabletEntityRangedValue[] rangedValue;
	// pen orientation, mouse orientation, ...
	private TabletToolRotation orientations[];
	// all buttons
	private boolean buttons[];
	
	
	TabletTool() {
	}
	
	
	public long getID() {
		return id;
	}
	
	
	public TabletDevice getTabletDevice() {
		return tabletDevice;
	}
	
	
	public double getX() {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public double getY() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public double getZ() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public TabletToolRotation getTilt() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public double getPressure(int index) {
		return pressures[index];
	}


	public boolean getButton(int index) {
		return buttons[index];
	}
	
	
	public TabletEntityRangedValue getRangedValue(int index) {
		return rangedValue[index];
	}

	
}
