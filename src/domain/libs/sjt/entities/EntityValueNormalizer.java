/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.entities;

import domain.libs.sjt.impl.TabletImplementationException;

// out range 0.0d to 1.0d
public class EntityValueNormalizer {
	
	private final double offset;
	private final double offsettedMax;
	private final double translatedCenter;
	private final boolean invert;
	
	
	public EntityValueNormalizer(double rangeStart, double rangeEnd, double center) throws TabletImplementationException {
		if (rangeStart == rangeEnd) throw new TabletImplementationException("rangeStart == rangeEnd.");

		if (rangeStart < rangeEnd) {
			offset = -rangeStart;
			offsettedMax = rangeEnd + offset;
			invert = false;
		}
		else {
			offset = -rangeEnd;
			offsettedMax = rangeStart + offset;
			invert = true;
		}
		
		translatedCenter = translateValue(center);
	}


	public double getCenter() {
		return translatedCenter;
	}


	public final double translateValue(double rawValue) throws TabletImplementationException {
		final double translatedValue = ((rawValue + offset) / offsettedMax);
		
		if (invert) return 1 - translatedValue;
		else return translatedValue;
	}
	
	
}
