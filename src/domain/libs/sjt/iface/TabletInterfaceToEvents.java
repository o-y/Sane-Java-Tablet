/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.iface;

import domain.libs.sjt.entities.TabletDevice;
import domain.libs.sjt.entities.TabletTool;
import domain.libs.sjt.misc.ConsoleLogger;


public class TabletInterfaceToEvents implements TabletBaseInterface {
	
	private final ConsoleLogger log = ConsoleLogger.newInstanceForCallingClass();
	private TabletEventInterface tabletEventInterface;
	
	public TabletInterfaceToEvents(TabletEventInterface tabletEventInterface) {
		this.tabletEventInterface = tabletEventInterface;
	}

	
	@Override
	public void tabletActivity(
			TabletDevice tabletDevice,
			TabletTool tabletTool,
			int indexes[],
			TabletActivityType... activities
	) {
		for (TabletActivityType activity : activities) {
			dispatchActivity(tabletDevice, tabletTool, indexes[activity.ordinal()], activity);
		}
	} // method


	private void dispatchActivity(TabletDevice tabletDevice, TabletTool tabletTool, int index, TabletActivityType activity) {
		switch (activity) {
		
		case TOOL_ENTER:
			tabletEventInterface.tabletToolEnter(tabletTool);
			break;
		
		case TOOL_EXIT:
			tabletEventInterface.tabletToolExit(tabletTool);
			break;
		
		case TOOL_POSITION_XY:
			tabletEventInterface.tabletToolPositionXY(tabletTool);
			break;
		
		case TOOL_POSITION_Z:
			tabletEventInterface.tabletToolPositionZ(tabletTool);
			break;
		
		case TOOL_TILT:
			tabletEventInterface.tabletToolTilt(tabletTool);
			break;
		
		case TOOL_PRESSURE:
			tabletEventInterface.tabletToolPressure(tabletTool, index);
			break;
			
		case TOOL_RANGEDVALUE:
			tabletEventInterface.tabletToolRangedValue(tabletTool, index);
			break;
		
		case TOOL_BUTTON:
			tabletEventInterface.tabletToolButton(tabletTool, index);
			break;

		case TABLET_RANGEDVALUE:
			tabletEventInterface.tabletRangedValue(tabletDevice, index);
			break;
		
		case TABLET_BUTTON:
			tabletEventInterface.tabletButton(tabletDevice, index);
			break;
			
		default:
			log.error(
					"Received unknown type of event (%d:%s) from library. Couldn't translate, ignored.",
					activity.ordinal(),
					activity.name());
		}
	} // method

	
} // class
