/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.misc;

import java.util.Arrays;
import java.util.LinkedList;

public class ConsoleLogger {
	
	private final String prefix;
	private static volatile long loggerInitTime = System.currentTimeMillis();
	private final LinkedList<Object> argsList = new LinkedList<Object>();
	private static final boolean debugEnabled = true; //!"true".equals(System.getProperty("SaneJavaTablet.debugEnabled"));

	
	public static ConsoleLogger newInstanceForCallingClass() {
		StackTraceElement[] es = new Throwable().getStackTrace();
		return new ConsoleLogger(getOnlyClassName(es[1].getClassName()));
	}
	
	
	private static String getOnlyClassName(String className) {
		return className.replaceFirst("^.*\\.([^\\.]*)$", "$1");
	}


	public ConsoleLogger(String prefix) {
		this.prefix = prefix;
	}
	
	
	public void info(String format, Object... args) {
		printOut("INFO", format, args);
	}
	
	
	public void error(String format, Object... args) {
		printOut("ERROR", format, args);
	}
	

	public void debug(String format, Object... args) {
		if (!debugEnabled) return;
		printOut("DEBUG", format, args);
	}
	

	private void printOut(String type, String format, Object... args) {
		argsList.clear();
		argsList.add((System.currentTimeMillis() - loggerInitTime) / 1000);
		argsList.add(type);
		argsList.add(prefix);
		argsList.addAll(Arrays.asList(args));
		System.out.println(String.format(
				"+%ds [%s %s] " + format,
				argsList.toArray()
		));
	}


} // class
