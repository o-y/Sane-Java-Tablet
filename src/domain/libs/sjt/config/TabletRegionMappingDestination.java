/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.config;

import java.awt.geom.Rectangle2D;

public class TabletRegionMappingDestination {
	
	private Rectangle2D.Double region;
	
	
	public TabletRegionMappingDestination(Rectangle2D.Double region) {
		this.region = region;
	}
	
	
	/*
	 * map to
	 * - app specified Component
	 * - display where application specified Component is
	 * - display region
	 */

}
