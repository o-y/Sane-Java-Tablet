/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.config;

public class TabletToolButtonMapping {
	
	public static enum Type {
		// if button can be left unreserved, so will be done. otherwise this value is invalid. fall-to disabled
		LET_OS_DECIDE,
		// button activity will be forwarded to the program
		LET_APP_DECIDE,
		// activates specified region mapping -configuration 
		ACTIVATE_REGIONCONFIG,
		// activates specified button mapping -configuration 
		ACTIVATE_BUTTONCONFIG,

		DISABLED
	}
	
	private Type mappingType;
	private String data;
	
	
	public TabletToolButtonMapping() {
		mappingType = Type.DISABLED;
		data = "";
	}
	
	
	public Type getMappingType() {
		return mappingType;
	}
	
	
	public void setMappingType(Type mappingType) {
		this.mappingType = mappingType;
	}
	
	
	public String getData() {
		return data;
	}

	
	public void setData(String data) {
		this.data = data;
	}
	
	
}
