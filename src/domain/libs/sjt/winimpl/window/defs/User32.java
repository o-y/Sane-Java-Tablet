/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.winimpl.window.defs;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.WString;
import com.sun.jna.win32.StdCallLibrary;


public interface User32 extends StdCallLibrary {
	
	User32 INSTANCE = (User32)Native.loadLibrary("user32", User32.class);
	
	
	/*	HCURSOR WINAPI LoadCursor(
			__in_opt  HINSTANCE hInstance,
			__in      LPCTSTR lpCursorName		*/
	Pointer LoadCursorW(Pointer hInstance, WString lpCursorName);
	Pointer LoadCursorW(Pointer hInstance, Pointer lpCursorName);
	
	
	/*	HICON WINAPI LoadIcon(
			__in_opt  HINSTANCE hInstance,
			__in      LPCTSTR lpIconName		*/
	Pointer LoadIconW(Pointer hInstance, WString lpIconName);
	Pointer LoadIconW(Pointer hInstance, Pointer lpIconName);
	
	
	/*	ATOM WINAPI RegisterClassEx(
			__in  const WNDCLASSEX *lpwcx		*/
	short RegisterClassExW(WNDCLASSEX lpwcx);
	
	
	/*	HWND WINAPI CreateWindowEx(
		  __in      DWORD dwExStyle,
		  __in_opt  LPCTSTR lpClassName,
		  __in_opt  LPCTSTR lpWindowName,
		  __in      DWORD dwStyle,
		  __in      int x,
		  __in      int y,
		  __in      int nWidth,
		  __in      int nHeight,
		  __in_opt  HWND hWndParent,
		  __in_opt  HMENU hMenu,
		  __in_opt  HINSTANCE hInstance,
		  __in_opt  LPVOID lpParam		*/
	Pointer CreateWindowExW(
			int dwExStyle,
			WString lpClassName,
			WString lpWindowName,
			int dwStyle,
			int x, int y, int nWidth, int nHeight,
			Pointer hWndParent,
			Pointer hMenu,
			Pointer hInstance,
			Pointer lpParam);
	
	
	/*	BOOL UpdateWindow(
  			__in  HWND hWnd		*/
	int UpdateWindow(Pointer hWnd);
	
	
	/*	BOOL ShowWindow( 
			HWND hWnd, 
			int nCmdShow		*/ 
	int ShowWindow(Pointer hWnd, int nCmdShow);
	
	
	/*	LRESULT WINAPI SendMessage(		// LRESULT = "long" @ 32bit. Problems?
		__in  HWND hWnd,
		__in  UINT Msg,
		__in  WPARAM wParam,
		__in  LPARAM lParam		*/		// LPARAM = "long" @ 32bit. Problems?
	Pointer SendMessageW(Pointer hWnd, int msg, Pointer wParam, Pointer lParam);
	
	
	/*	BOOL WINAPI PostMessage(
		__in_opt  HWND hWnd,
		__in      UINT Msg,
		__in      WPARAM wParam,
		__in      LPARAM lParam		*/		// LPARAM = "long" @ 32bit. Problems?
	int PostMessageW(Pointer hWnd, int msg, Pointer wParam, Pointer lParam);
	
	
	/*	VOID WINAPI PostQuitMessage(
			__in  int nExitCode		*/
	void PostQuitMessage(int nExitCode);
	
	
	/*	LRESULT WINAPI DefWindowProc(		// LRESULT = "long" @ 32bit. Problems?
			__in  HWND hWnd,
			__in  UINT Msg,
			__in  WPARAM wParam,
			__in  LPARAM lParam		*/		// LPARAM = "long" @ 32bit. Problems?
	Pointer DefWindowProcW(Pointer hWnd, int msg, Pointer wParam, Pointer lParam);
	
	
	/*	BOOL WINAPI GetMessage(
			__out     LPMSG lpMsg,
			__in_opt  HWND hWnd,
			__in      UINT wMsgFilterMin,
			__in      UINT wMsgFilterMax		*/
	int GetMessageW(MSGByReference lpMsg, Pointer hWnd, int wMsgFilterMin, int wMsgFilterMax);
	int GetMessageW(Pointer lpMsg, Pointer hWnd, int wMsgFilterMin, int wMsgFilterMax);
	
	
	/*	BOOL WINAPI TranslateMessage(
			__in  const MSG *lpMsg		*/
	int TranslateMessage(MSGByReference lpMsg);
	int TranslateMessage(Pointer lpMsg);
	
	
	/*	LRESULT WINAPI DispatchMessage(		// LRESULT = "long" @ 32bit. Problems?
			__in  const MSG *lpmsg		*/
	Pointer DispatchMessageW(MSGByReference lpmsg);
	Pointer DispatchMessageW(Pointer lpmsg);
	
	
	/* DEFINITIONS BELOW */
	
	public static long IDI_APPLICATION = 32512L;
	public static long IDC_ARROW = 32512L;
	public static long COLOR_APPWORKSPACE = 12L;
	public static long WS_OVERLAPPED = 0x00000000L;
	public static long WS_CAPTION = 0x00C00000L;
	public static long WS_SYSMENU = 0x00080000L;
	public static long WS_THICKFRAME = 0x00040000L;
	public static long WS_MINIMIZEBOX = 0x00020000L;
	public static long WS_MAXIMIZEBOX = 0x00010000L;
	public static long WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME| WS_MINIMIZEBOX | WS_MAXIMIZEBOX;
	public static long CW_USEDEFAULT = 0x80000000L;
	public static long SW_SHOWNORMAL = 1L;
	public static long WM_CREATE = 1L;
	public static long WM_ACTIVATE = 6L;
	public static long WM_DESTROY = 2L;
	public static long WM_USER = 0x0400L;
	public static long WM_USER_CLOSE_IMPLEMENTATION =	WM_USER + 1790;
	public static long WM_USER_ENABLE_IMPLEMENTATION =	WM_USER + 1791;
	public static long WM_USER_DISABLE_IMPLEMENTATION =	WM_USER + 1792;
	public static long WM_USER_ACTIVATE_NEW_SETTINGS =	WM_USER + 1793;
	public static Pointer POINTER_ZERO = new Pointer(0);
	



}
