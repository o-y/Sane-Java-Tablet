/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.winimpl;

import java.util.concurrent.Exchanger;

import domain.libs.sjt.impl.TabletImplementation;
import domain.libs.sjt.impl.TabletImplementationException;

public class TabletWinImplWrapper extends Thread {

	private final Exchanger<TabletImplementationException> exceptionDispatcher;
	private volatile TabletWinImpl impl;
	private final boolean grab;

	
	public TabletWinImplWrapper(boolean grab) {
		this.grab = grab;
		exceptionDispatcher = new Exchanger<TabletImplementationException>();
	}

	
	public static TabletImplementation createImplementation(boolean grab) throws TabletImplementationException {
		TabletWinImplWrapper wrapper = new TabletWinImplWrapper(grab);
		wrapper.start();
		wrapper.throwExceptions();
		return wrapper.getImplementation();
	}
	
	
	@Override
	public void run() {
		try {
			impl = new TabletWinImpl(grab);
			dispatchNoExceptions();
			impl.runImplementation();
		}
		catch (TabletImplementationException e) {
			dispatchException(e);
		}
	}
	
	
	public void throwExceptions() throws TabletImplementationException {
		try {
			TabletImplementationException exception = exceptionDispatcher.exchange(null);
			if (exception != null) throw exception;
		}
		catch (InterruptedException e) {
			new TabletImplementationException(e,
					"Interrupted while trying to retrieve exception."
					).printStackTrace();
		}
	} // method
	
	
	private TabletWinImpl getImplementation() {
		return impl;
	}


	private void dispatchException(TabletImplementationException e) {
		try {
			exceptionDispatcher.exchange(e);
		} catch (InterruptedException e2) {
			new TabletImplementationException(e,
					"Interrupted while trying to deliver an exception."
					).printStackTrace();
		}
	}
	
	
	private void dispatchNoExceptions() {
		dispatchException(null);
	}


}
