/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.winimpl.wintab.defs;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class PACKET extends Structure {
	
	/* STRUCTURE AND DATA TYPES VERIFIED */
	
	
	/*	Pointer			pkContext;
		UINT			pkStatus;
		DWORD			pkTime;
		WTPKT			pkChanged;
		UINT			pkSerialNumber;
		UINT			pkCursor;
		DWORD			pkButtons;
		LONG			pkX;
		LONG			pkY;
		LONG			pkZ;
		int				pkNormalPressure;
		int				pkTangentPressure;
		ORIENTATION		pkOrientation;
		ROTATION		pkRotation; // 1.1 		*/

	public Pointer pkContext;
	public int pkStatus;
	public int pkTime;
	public int pkChanged;
	public int pkSerialNumber;
	public int pkCursor;
	public int pkButtons;
	public int pkX;
	public int pkY;
	public int pkZ;
	public int pkNormalPressure;
	public int pkTangentPressure;
	public ORIENTATION pkOrientation;
	public ROTATION pkRotation;

	
	public PACKET() {
		super();
	}
	
	
	public PACKET(Pointer p) {
		useMemory(p);
		read();
	}
	
	
}
