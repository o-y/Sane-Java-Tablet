/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.winimpl.wintab.defs;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.win32.StdCallLibrary;

public interface Wintab32 extends StdCallLibrary {
	
	Wintab32 INSTANCE = (Wintab32)Native.loadLibrary("Wintab32", Wintab32.class);
	
	
	/*	UINT API WTInfoW(UINT, UINT, LPVOID);		*/
	int WTInfoW(int wCategory, int nIndex, Pointer lpOutput);
	int WTInfoW(int wCategory, int nIndex, LOGCONTEXTW lpOutput);
	int WTInfoW(int wCategory, int nIndex, AXIS lpOutput);
//	int WTInfoW(int wCategory, int nIndex, IntByReference lpOutput);
//	int WTInfoW(int wCategory, int nIndex, char[] lpOutput);
//	int WTInfoW(int wCategory, int nIndex, int lpOutput);
	int WTInfoW(int wCategory, int nIndex, short[] lpOutput);
	int WTInfoW(int wCategory, int nIndex, int[] lpOutput);
	int WTInfoW(int wCategory, int nIndex, String lpOutput);
	
	
	/*	HCTX API WTOpenW(HWND, LPLOGCONTEXTW, BOOL);		*/
	Pointer WTOpenW(Pointer hWnd, LOGCONTEXTW lpLogCtx, int fEnable);
	
	
	/*	BOOL API WTEnable(HCTX, BOOL);		*/
	int WTEnable(Pointer hCtx, int fEnable);
	
	
	/*	BOOL API WTOverlap(HCTX, BOOL);		*/
	int WTOverlap(Pointer hCtx, int fEnable);
	
	
	/*	BOOL API WTClose(HCTX);		*/
	int WTClose(Pointer hCtx);
	
	
//	/*	int API WTPacketsGet(HCTX, int, LPVOID);		*/
//	int WTPacketsGet(Pointer hCtx, int MaxPackets, Pointer lpPkts);
	
	
//	/*	BOOL API WTPacket(HCTX, UINT, LPVOID);		*/
//	int WTPacket(Pointer hCtx, int wSerial, Pointer lpPkt);
	
	
//	/*	BOOL API WTGetW(HCTX, LPLOGCONTEXTW);		*/
//	int WTGetW(Pointer hCtx, LOGCONTEXTW lpLogCtx);
	
	
//	/*	BOOL API WTConfig(HCTX, HWND);		*/
//	int WTConfig(Pointer hCtx, Pointer hWnd);
	
	
//	/*	BOOL API WTExtGet(HCTX, UINT, LPVOID);		*/
//	int WTExtGet(Pointer hCtx, int wExt, Pointer lpData);
//	int WTExtGet(Pointer hCtx, int wExt, byte[] lpData);
	
	
//	/*	BOOL API WTExtSet(HCTX, UINT, LPVOID);		*/
//	int WTExtSet(Pointer hCtx, int wExt, Pointer lpData);
//	int WTExtSet(Pointer hCtx, int wExt, byte[] lpData);
	
	/*	BOOL API WTQueueSizeSet(HCTX, int);		*/
	int WTQueueSizeSet(Pointer hCtx, int nPkts);
	
	
	/*	DEFINITIONS BELOW	*/
	
	public static long LCNAMELEN = 40L;
	public static long WT_DEFBASE = 0x7FF0L;
	public static long WT_PACKET = WT_DEFBASE + 0L;
	public static long CXO_SYSTEM = 1L;
	public static long CXO_MESSAGES = 4L;
	public static long CXO_CSRMESSAGES = 8L;
	public static long CXS_ONTOP = 4L;
	public static long CXL_INSIZE = 1L;
	public static long CXL_INASPECT = 2L;
	public static long PK_CONTEXT 			= 0x0001L;	/* reporting context */
	public static long PK_STATUS 			= 0x0002L;	/* status bits */
	public static long PK_TIME 				= 0x0004L;	/* time stamp */
	public static long PK_CHANGED 			= 0x0008L;	/* change bit vector */
	public static long PK_SERIAL_NUMBER 	= 0x0010L;	/* packet serial number */
	public static long PK_CURSOR 			= 0x0020L;	/* reporting cursor */
	public static long PK_BUTTONS 			= 0x0040L;	/* button information */
	public static long PK_X 				= 0x0080L;	/* x axis */
	public static long PK_Y 				= 0x0100L;	/* y axis */
	public static long PK_Z 				= 0x0200L;	/* z axis */
	public static long PK_NORMAL_PRESSURE 	= 0x0400L;	/* normal or tip pressure */
	public static long PK_TANGENT_PRESSURE 	= 0x0800L;	/* tangential or barrel pressure */
	public static long PK_ORIENTATION 		= 0x1000L;	/* orientation info: tilts */
	public static long PK_ROTATION			= 0x2000L;	/* rotation info; 1.1 */
	
	public static long WTI_DEVICES = 100L;
	public static long WTI_INTERFACE = 1L;
	public static long IFC_SPECVERSION = 2L;
	public static long IFC_NDEVICES = 4L;
	public static long IFC_NCURSORS = 5L;
	public static long DVC_NAME = 1L;
	public static long DVC_PKTRATE = 5L;
	public static long DVC_XMARGIN = 9L;
	public static long DVC_YMARGIN = 10L;
	public static long DVC_X = 12L;
	public static long DVC_Y = 13L;
	public static long DVC_Z = 14L;
	public static long DVC_PNPID = 19L;
	public static long WTI_DEFCONTEXT = 3L;
	public static long WTI_DEFSYSCTX = 4L;
	public static long WTI_DDCTXS = 400L; /* 1.1 */
	public static long WTI_DSCTXS = 500L; /* 1.1 */
	public static long WTI_CURSORS = 200L;
	public static long CSR_NAME = 1L;
	public static long WTX_CSRMASK = 3L;
	public static long WTI_EXTENSIONS = 300L;
	public static long EXT_TAG = 2L;
	public static long EXT_NAME = 1L;
	
	public static long CTX_INORGX = 13L;
	public static long CTX_INORGY = 14L;
	public static long CTX_INORGZ = 15L;
	public static long CTX_INEXTX = 16L;
	public static long CTX_INEXTY = 17L;
	public static long CTX_INEXTZ = 18L;
	public static long CTX_OUTORGX = 19L;
	public static long CTX_OUTORGY = 20L;
	public static long CTX_OUTORGZ = 21L;
	public static long CTX_OUTEXTX = 22L;
	public static long CTX_OUTEXTY = 23L;
	public static long CTX_OUTEXTZ = 24L;
	public static long CTX_SYSORGX = 29L;
	public static long CTX_SYSORGY = 30L;
	public static long CTX_SYSEXTX = 31L;
	public static long CTX_SYSEXTY = 32L;

}
