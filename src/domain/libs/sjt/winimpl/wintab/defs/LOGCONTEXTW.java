/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.winimpl.wintab.defs;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class LOGCONTEXTW extends Structure {
	
	/* STRUCTURE AND DATA TYPES VERIFIED */
	
	
	/*	WCHAR	lcName[LCNAMELEN];
		UINT	lcOptions;
		UINT	lcStatus;
		UINT	lcLocks;
		UINT	lcMsgBase;
		UINT	lcDevice;
		UINT	lcPktRate;
		WTPKT	lcPktData;
		WTPKT	lcPktMode;
		WTPKT	lcMoveMask;
		DWORD	lcBtnDnMask;
		DWORD	lcBtnUpMask;
		LONG	lcInOrgX;
		LONG	lcInOrgY;
		LONG	lcInOrgZ;
		LONG	lcInExtX;
		LONG	lcInExtY;
		LONG	lcInExtZ;
		LONG	lcOutOrgX;
		LONG	lcOutOrgY;
		LONG	lcOutOrgZ;
		LONG	lcOutExtX;
		LONG	lcOutExtY;
		LONG	lcOutExtZ;
		FIX32	lcSensX;
		FIX32	lcSensY;
		FIX32	lcSensZ;
		BOOL	lcSysMode;
		int		lcSysOrgX;
		int		lcSysOrgY;
		int		lcSysExtX;
		int		lcSysExtY;
		FIX32	lcSysSensX;
		FIX32	lcSysSensY;		*/
	
	public char lcName[] = new char[(int) Wintab32.LCNAMELEN];
	public int lcOptions;        
	public int lcStatus;         
	public int lcLocks;          
	public int lcMsgBase;        
	public int lcDevice;         
	public int lcPktRate;        
	public int lcPktData;        
	public int lcPktMode;        
	public int lcMoveMask;       
	public int lcBtnDnMask;      
	public int lcBtnUpMask;      
	public int lcInOrgX;         
	public int lcInOrgY;         
	public int lcInOrgZ;         
	public int lcInExtX;         
	public int lcInExtY;         
	public int lcInExtZ;         
	public int lcOutOrgX;        
	public int lcOutOrgY;        
	public int lcOutOrgZ;        
	public int lcOutExtX;        
	public int lcOutExtY;        
	public int lcOutExtZ;        
	public int lcSensX;          
	public int lcSensY;          
	public int lcSensZ;          
	public int lcSysMode;        
	public int lcSysOrgX;        
	public int lcSysOrgY;        
	public int lcSysExtX;        
	public int lcSysExtY;        
	public int lcSysSensX;       
	public int lcSysSensY;
	

	public LOGCONTEXTW() {
		super();
	}

	public LOGCONTEXTW(Pointer p) {
		useMemory(p);
		read();
	}

}
