/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.winimpl.wintab.defs;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

/* hoping that since wacom says if LOGCONTEXT.lcData fields are set, then the ones
 * not available will be ignored. wouldnt be nice to have to parse this.
 * plus it's optimising to not have jna instantiate a new class for all of the members
 * of this struct for every PACKETEXT.
 * i also assume that if the wacom "driver's" wintab implementation supports
 * EXPKEYSDATA, TOUCHSTRIP and TOUCHRING, the driver doesn't bother to fail on wtopen
 * even if the requested tablet(ID) doesn't support them.
 * as they say: "insert quote here about the PACKETDATA not
 * supported items doesn't seem to fail"
 */
public class PACKETEXT extends Structure { /* 1.4 */

	/* STRUCTURE AND DATA TYPES VERIFIED */
	
	
	/* EXTENSIONBASE */
	/*	HCTX		nContext;
		UINT		nStatus;
		DWORD		nTime;
		UINT		nSerialNumber;		*/
	
	public Pointer	pkBase_nContext;
	public int		pkBase_nStatus;
	public int		pkBase_nTime;
	public int		pkBase_nSerialNumber;
	
	
	/* EXPKEYSDATA */
	/*	BYTE		nTablet;
		BYTE		nControl;
		BYTE		nLocation;
		BYTE		nReserved;
		DWORD		nState;		*/
	
	public byte pkExpKeys_nTablet;
	public byte pkExpKeys_nControl;
	public byte pkExpKeys_nLocation;
	public byte pkExpKeys_nReserved;
	public int  pkExpKeys_nState;
	
	
	/* SLIDERDATA (TOUCHSTRIP) */
	/*	BYTE		nTablet;
		BYTE		nControl;
		BYTE		nMode;
		BYTE		nReserved;
		DWORD		nPosition;		*/
	
	public byte pkTouchStrip_nTablet;
	public byte pkTouchStrip_nControl;
	public byte pkTouchStrip_nMode;
	public byte pkTouchStrip_nReserved;
	public int  pkTouchStrip_nPosition;
	
	
	/* SLIDERDATA (TOUCHRING) */
	/*	BYTE		nTablet;
		BYTE		nControl;
		BYTE		nMode;
		BYTE		nReserved;
		DWORD		nPosition;		*/
	
	public byte pkTouchRing_nTablet;
	public byte pkTouchRing_nControl;
	public byte pkTouchRing_nMode;
	public byte pkTouchRing_nReserved;
	public int  pkTouchRing_nPosition;
	
	
	public PACKETEXT() {
		super();
	}

	public PACKETEXT(Pointer p) {
		useMemory(p);
		read();
	}

}
