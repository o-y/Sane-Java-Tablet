/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.winimpl;

import com.sun.jna.Native;
import com.sun.jna.Pointer;

import domain.libs.sjt.impl.TabletImplementation;
import domain.libs.sjt.misc.ConsoleLogger;
import domain.libs.sjt.winimpl.window.defs.User32;
import domain.libs.sjt.winimpl.wintab.defs.PACKET;


public class TabletWinImpl extends TabletImplementation {
	
	private final ConsoleLogger log = ConsoleLogger.newInstanceForCallingClass();
	protected final TabletWinImplWinFuncs winFuncs;
	protected final TabletWinImplWintabFuncs wtFuncs;
	
	
	public TabletWinImpl(boolean grab) throws TabletWinImplException {
		super(grab);
		Native.setProtected(true);
		log.debug("Initializing tablet implementation.");
		
		winFuncs = new TabletWinImplWinFuncs(this);
		wtFuncs = new TabletWinImplWintabFuncs(this);
		
		wtFuncs.ensureWintabIsPresent();
		
		winFuncs.registerClassEx();
		winFuncs.createWindowEx();
		try {
			wtFuncs.initTablets();
		}
		catch (TabletWinImplException e) {
			wtFuncs.tabletCleanup();
			throw e;
		}
		
		System.gc();
		
//		User32.INSTANCE.ShowWindow(hWnd, (int) User32.SW_SHOWNORMAL);
//		User32.INSTANCE.UpdateWindow(hWnd);
		
//		if (1==1)throw new TabletImplementationException("lol");
//		try {
//			window = new WindowsImplWindow(this);
//			winTab = new WindowsImplWinTab(this);
//		}
//		catch (TabletImplementationException e) {
//			closeImplementation();
//			throw e;
//		}
	} // method
	
	
	public void runImplementation() {
		log.debug("Running implementation.");
		
		winFuncs.windowMessageLoop();
	}
	
	
	void handlePacket(int devIdx, PACKET packet) {
//		System.out.println(
//				String.format("%16s", Integer.toBinaryString(packet.pkZ)).replace(' ', '0') +
//				"     " + packet.pkZ
//		);
//		System.out.println(String.format(
//				"%d        %d        %d",
//				packet.pkOrientation.orAltitude,
//				packet.pkOrientation.orAzimuth,
//				packet.pkOrientation.orTwist
//				));
		System.out.println(String.format(
				"%d        %d",
				packet.pkX,
				packet.pkY
				));
	}


	void activateNewSettings() {
		// TODO Auto-generated method stub
		
	}


	// toisesta threadista!
	@Override
	public void closeImplementation() {
		User32.INSTANCE.PostMessageW(winFuncs.hWnd, (int) User32.WM_DESTROY, Pointer.NULL, Pointer.NULL);
	} // method


	// toisesta threadista!
	@Override
	public void enableImplementation() {
		User32.INSTANCE.PostMessageW(winFuncs.hWnd, (int) User32.WM_USER_ENABLE_IMPLEMENTATION, Pointer.NULL, Pointer.NULL);
	}


	// toisesta threadista!
	@Override
	public void disableImplementation() {
		User32.INSTANCE.PostMessageW(winFuncs.hWnd, (int) User32.WM_USER_DISABLE_IMPLEMENTATION, Pointer.NULL, Pointer.NULL);
	}


	// toisesta threadista!
	@Override
	protected void useNewSettings() {
		User32.INSTANCE.PostMessageW(winFuncs.hWnd, (int) User32.WM_USER_ACTIVATE_NEW_SETTINGS, Pointer.NULL, Pointer.NULL);
	}


} // class



