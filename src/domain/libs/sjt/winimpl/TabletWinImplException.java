/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.winimpl;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.Kernel32Util;

import domain.libs.sjt.impl.TabletImplementationException;

public class TabletWinImplException extends TabletImplementationException {

	private static final long serialVersionUID = 1L;
	

	public TabletWinImplException(String format, Object... args) {
		this(null, format, args);
	}

	public TabletWinImplException(Throwable e, String format, Object... args) {
		super(String.format(
				"%s Last error was '%s'",
				Kernel32Util.formatMessageFromLastErrorCode(Native.getLastError())
				), format, args);
	}

}
