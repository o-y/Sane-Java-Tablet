/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt;

import java.util.LinkedList;

import com.sun.jna.Native;

import domain.libs.sjt.config.TabletConfigurationException;
import domain.libs.sjt.config.TabletConfigurations;
import domain.libs.sjt.impl.TabletImplementation;
import domain.libs.sjt.impl.TabletImplementationException;
import domain.libs.sjt.winimpl.TabletWinImplWrapper;

public class SaneJavaTablet {
	
	private TabletImplementation tabletImplementation;

	
	public SaneJavaTablet(boolean grab) throws TabletImplementationException, TabletConfigurationException {
		initialize(grab);
	}
	
	
	private void initialize(boolean grab) throws TabletImplementationException {
		Native.setProtected(true); // decide: probably better to be global
//		Native.set auto structure update...
		final String osName = System.getProperty("os.name").toLowerCase();
		
		if (osName.startsWith("windows")) {
			tabletImplementation = TabletWinImplWrapper.createImplementation(grab);
		}
		else {
			throw new TabletImplementationException(
					"SaveJavaTablet doesn't support this (%s) platform",
					osName);
		}
		
	} // method
	
	
	public void useSettings(TabletConfigurations tabletConfigs, LinkedList<TabletComponentLink> listenOnComponents) {
		tabletImplementation.useSettings(tabletConfigs, listenOnComponents);
	}

	
	@Override
	protected void finalize() throws Throwable {
		tabletImplementation.closeImplementation();
	}
	
	
	// can be used to test if swing mouse move and button press events should be ignored
	public boolean isToolOnTablet() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	public void enable() {
		tabletImplementation.enableImplementation();
	}


	public void disable() {
		tabletImplementation.disableImplementation();
	}


	public void close() {
		tabletImplementation.closeImplementation();
	}


}
