package domain.libs.sjt.garbage;

import com.sun.jna.IntegerType;

@SuppressWarnings("serial")
public class BOOL extends IntegerType {
	
	
	public BOOL() {
		super(4);
	}
	
	
	public BOOL(long value) {
		super(4, value);
	}
	
	
}
