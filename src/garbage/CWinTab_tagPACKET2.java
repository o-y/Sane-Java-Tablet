/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;

import net.screwbox.libs.sanejavatablet.windows.wintab.CWinTab;

import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;


public class CWinTab_tagPACKET2 {
	
//	Pointer pkContext;
//	int pkStatus;
//	int pkTime;
//	int pkChanged;
//	int pkSerialNumber;
//	int pkCursor;
//	int pkButtons;
//	NativeLong pkX;
//	NativeLong pkY;
//	NativeLong pkZ;
//	int pkNormalPressure;
//	int pkTangentPressure;
//	ORIENTATION pkOrientation;
//	ROTATION pkRotation;
//	int pkFKeys;
//	TILT pkTilt;
	
	private Pointer structBaseAddress;
//	private int lcPktData;
//	private int lcPktMode;

	
	public void setStructView(Pointer structBaseAddress, int lcPktData, int lcPktMode) {
		this.structBaseAddress = structBaseAddress;
//		this.lcPktData = lcPktData;
//		this.lcPktMode = lcPktMode;
	}
	
	
	public Pointer get_pkContext() {
		return structBaseAddress.getPointer(calcOffset(CWinTab.PK_CONTEXT));
	}
	
	
	public int get_pkStatus() {
		return structBaseAddress.getInt(calcOffset(CWinTab.PK_STATUS));
	}

	
	public int get_pkTime() {
		return structBaseAddress.getInt(calcOffset(CWinTab.PK_TIME));
	}
	
	
	public int get_pkChanged() {
		return structBaseAddress.getInt(calcOffset(CWinTab.PK_CHANGED));
	}
	
	public int get_pkSerialNumber() {
		return structBaseAddress.getInt(calcOffset(CWinTab.PK_SERIAL_NUMBER));
	}
	
	
	public int get_pkCursor() {
		return structBaseAddress.getInt(calcOffset(CWinTab.PK_CURSOR));
	}
	
	
	public int get_pkButtons() {
		return structBaseAddress.getInt(calcOffset(CWinTab.PK_BUTTONS));
	}
	
	
	public NativeLong get_pkX() {
		return structBaseAddress.getNativeLong(calcOffset(CWinTab.PK_X));
	}
	
	
	public NativeLong get_pkY() {
		return structBaseAddress.getNativeLong(calcOffset(CWinTab.PK_Y));
	}
	
	
	public NativeLong get_pkZ() {
		return structBaseAddress.getNativeLong(calcOffset(CWinTab.PK_Z));
	}
	
	
	public int get_pkNormalPressure() {
		return structBaseAddress.getInt(calcOffset(CWinTab.PK_NORMAL_PRESSURE));
	}
	
	
	public int get_pkTangentPressure() {
		return structBaseAddress.getInt(calcOffset(CWinTab.PK_TANGENT_PRESSURE));
	}
	
	
	public CWinTab_tagORIENTATION get_pkOrientation() {
		return new CWinTab_tagORIENTATION(structBaseAddress.getPointer(calcOffset(CWinTab.PK_ORIENTATION)));
	}
	
	
	public CWinTab_tagROTATION get_pkRotation() {
		return new CWinTab_tagROTATION(structBaseAddress.getPointer(calcOffset(CWinTab.PK_ROTATION)));
	}
	
	
//	public int get_pkFKeys() {
//		return structBaseAddress.getInt(calcOffset(CWinTab.PK_F));
//	}
//
//	
//	public CWinTab_tagTILT get_pkTilt() {
//		return new CWinTab_tagTILT(structBaseAddress.getPointer(calcOffset(CWinTab.PK_BUTTONS)));
//	}
	
	
//	private static final int getFullOffset(int pk) {
//		switch (pk) {
//		case CWinTab.PK_CONTEXT:
//			return Native.POINTER_SIZE;
//		case CWinTab.PK_STATUS:
//		case CWinTab.PK_TIME:
//		case CWinTab.PK_CHANGED:
//		case CWinTab.PK_SERIAL_NUMBER:
//		case CWinTab.PK_CURSOR:
//		case CWinTab.PK_BUTTONS:
//			return 4;
//		}
////		case 
//		return -1;
//	}
	

	private long calcOffset(int pkContext) {
		// TODO Auto-generated method stub
		return 0;
	}

	
}
