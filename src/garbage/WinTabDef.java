/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;

public enum WinTabDef {
	CRC_AGGREGATE(2),
	CRC_INVERT(4),
	CRC_MULTIMODE(1),
	CSR_ACTIVE(2),
	CSR_BTNNAMES(6),
	CSR_BUTTONBITS(5),
	CSR_BUTTONMAP(7),
	CSR_BUTTONS(4),
	CSR_CAPABILITIES(19),
	CSR_MAX(20),
	CSR_MINBUTTONS(18),
	CSR_MINPKTDATA(17),
	CSR_MODE(16),
	CSR_NAME(1),
	CSR_NPBTNMARKS(10),
	CSR_NPBUTTON(9),
	CSR_NPRESPONSE(11),
	CSR_PHYSID(15),
	CSR_PKTDATA(3),
	CSR_SYSBTNMAP(8),
	CSR_TPBTNMARKS(13),
	CSR_TPBUTTON(12),
	CSR_TPRESPONSE(14),
	CSR_TYPE(20),
	CTX_BTNDNMASK(11),
	CTX_BTNUPMASK(12),
	CTX_DEVICE(6),
	CTX_INEXTX(16),
	CTX_INEXTY(17),
	CTX_INEXTZ(18),
	CTX_INORGX(13),
	CTX_INORGY(14),
	CTX_INORGZ(15),
	CTX_LOCKS(4),
	CTX_MAX(34),
	CTX_MOVEMASK(10),
	CTX_MSGBASE(5),
	CTX_NAME(1),
	CTX_OPTIONS(2),
	CTX_OUTEXTX(22),
	CTX_OUTEXTY(23),
	CTX_OUTEXTZ(24),
	CTX_OUTORGX(19),
	CTX_OUTORGY(20),
	CTX_OUTORGZ(21),
	CTX_PKTDATA(8),
	CTX_PKTMODE(9),
	CTX_PKTRATE(7),
	CTX_SENSX(25),
	CTX_SENSY(26),
	CTX_SENSZ(27),
	CTX_STATUS(3),
	CTX_SYSEXTX(31),
	CTX_SYSEXTY(32),
	CTX_SYSMODE(28),
	CTX_SYSORGX(29),
	CTX_SYSORGY(30),
	CTX_SYSSENSX(33),
	CTX_SYSSENSY(34),
	CXL_INASPECT(2),
	CXL_INSIZE(1),
	CXL_MARGIN(8),
	CXL_SENSITIVITY(4),
	CXL_SYSOUT(16),
	CXO_CSRMESSAGES(8),
	CXO_MARGIN(32768),
	CXO_MESSAGES(4),
	CXO_MGNINSIDE(16384),
	CXO_PEN(2),
	CXO_SYSTEM(1),
	CXS_DISABLED(1),
	CXS_OBSCURED(2),
	CXS_ONTOP(4),
	DVC_CSRDATA(8),
	DVC_FIRSTCSR(4),
	DVC_HARDWARE(2),
	DVC_MAX(19),
	DVC_NAME(1),
	DVC_NCSRTYPES(3),
	DVC_NPRESSURE(15),
	DVC_ORIENTATION(17),
	DVC_PKTDATA(6),
	DVC_PKTMODE(7),
	DVC_PKTRATE(5),
	DVC_PNPID(19),
	DVC_ROTATION(18),
	DVC_TPRESSURE(16),
	DVC_X(12),
	DVC_XMARGIN(9),
	DVC_Y(13),
	DVC_YMARGIN(10),
	DVC_Z(14),
	DVC_ZMARGIN(11),
	EXT_AXES(5),
	EXT_CURSORS(9),
	EXT_DEFAULT(6),
	EXT_DEFCONTEXT(7),
	EXT_DEFSYSCTX(8),
	EXT_DEVICES(110),
	EXT_MASK(3),
	EXT_MAX(210),
	EXT_NAME(1),
	EXT_SIZE(4),
	EXT_TAG(2),
	HWC_HARDPROX(4),
	HWC_INTEGRATED(1),
	HWC_PHYSID_CURSORS(8),
	HWC_TOUCH(2),
	IFC_CTXOPTIONS(7),
	IFC_CTXSAVESIZE(8),
	IFC_IMPLVERSION(3),
	IFC_MAX(10),
	IFC_NCONTEXTS(6),
	IFC_NCURSORS(5),
	IFC_NDEVICES(4),
	IFC_NEXTENSIONS(9),
	IFC_NMANAGERS(10),
	IFC_SPECVERSION(2),
	IFC_WINTABID(1),
	LCNAMELEN(40),
	LC_NAMELEN(40),
	ORD_WTClose(22),
	ORD_WTConfig(60),
	ORD_WTDataGet(81),
	ORD_WTDataPeek(82),
	ORD_WTEnable(40),
	ORD_WTExtGet(63),
	ORD_WTExtSet(64),
	ORD_WTGet(61),
	ORD_WTInfo(20),
	ORD_WTMgrClose(101),
	ORD_WTMgrConfigReplace(141),
	ORD_WTMgrConfigReplaceEx(202),
	ORD_WTMgrContextEnum(120),
	ORD_WTMgrContextOwner(121),
	ORD_WTMgrCsrButtonMap(182),
	ORD_WTMgrCsrEnable(181),
	ORD_WTMgrCsrExt(185),
	ORD_WTMgrCsrPressureBtnMarks(183),
	ORD_WTMgrCsrPressureBtnMarksEx(201),
	ORD_WTMgrCsrPressureResponse(184),
	ORD_WTMgrDefContext(122),
	ORD_WTMgrDefContextEx(206),
	ORD_WTMgrDeviceConfig(140),
	ORD_WTMgrExt(180),
	ORD_WTMgrOpen(100),
	ORD_WTMgrPacketHook(160),
	ORD_WTMgrPacketHookDefProc(161),
	ORD_WTMgrPacketHookEx(203),
	ORD_WTMgrPacketHookNext(205),
	ORD_WTMgrPacketUnhook(204),
	ORD_WTOpen(21),
	ORD_WTOverlap(41),
	ORD_WTPacket(24),
	ORD_WTPacketsGet(23),
	ORD_WTPacketsPeek(80),
	ORD_WTQueuePackets(83),
	ORD_WTQueuePacketsEx(200),
	ORD_WTQueueSizeGet(84),
	ORD_WTQueueSizeSet(85),
	ORD_WTRestore(66),
	ORD_WTSave(65),
	ORD_WTSet(62),
	PKEXT_ABSOLUTE(1),
	PKEXT_RELATIVE(2),
	PK_BUTTONS(64),
	PK_CHANGED(8),
	PK_CONTEXT(1),
	PK_CURSOR(32),
	PK_NORMAL_PRESSURE(1024),
	PK_ORIENTATION(4096),
	PK_ROTATION(8192),
	PK_SERIAL_NUMBER(16),
	PK_STATUS(2),
	PK_TANGENT_PRESSURE(2048),
	PK_TIME(4),
	PK_X(128),
	PK_Y(256),
	PK_Z(512),
	SBN_LCLICK(1),
	SBN_LDBLCLICK(2),
	SBN_LDRAG(3),
	SBN_MCLICK(7),
	SBN_MDBLCLICK(8),
	SBN_MDRAG(9),
	SBN_NONE(0),
	SBN_P1CLICK(112),
	SBN_P1DBLCLICK(128),
	SBN_P1DRAG(144),
	SBN_P2CLICK(160),
	SBN_P2DBLCLICK(176),
	SBN_P2DRAG(192),
	SBN_P3CLICK(208),
	SBN_P3DBLCLICK(224),
	SBN_P3DRAG(240),
	SBN_PNCLICK(64),
	SBN_PNDBLCLICK(80),
	SBN_PNDRAG(96),
	SBN_PTCLICK(16),
	SBN_PTDBLCLICK(32),
	SBN_PTDRAG(48),
	SBN_RCLICK(4),
	SBN_RDBLCLICK(5),
	SBN_RDRAG(6),
	STA_BUTTONUSE(7),
	STA_CONTEXTS(1),
	STA_MANAGERS(5),
	STA_MAX(8),
	STA_PKTDATA(4),
	STA_PKTRATE(3),
	STA_SYSBTNUSE(8),
	STA_SYSCTXS(2),
	STA_SYSTEM(6),
	TABLET_LOC_BOTTOM(3),
	TABLET_LOC_LEFT(0),
	TABLET_LOC_RIGHT(1),
	TABLET_LOC_TOP(2),
	TABLET_LOC_TRANSDUCER(4),
	TABLET_PROPERTY_AVAILABLE(2),
	TABLET_PROPERTY_CONTROLCOUNT(0),
	TABLET_PROPERTY_FUNCCOUNT(1),
	TABLET_PROPERTY_LOCATION(11),
	TABLET_PROPERTY_MAX(4),
	TABLET_PROPERTY_MIN(3),
	TABLET_PROPERTY_OVERRIDE(5),
	TABLET_PROPERTY_OVERRIDE_NAME(6),
	TBN_DOWN(2),
	TBN_NONE(0),
	TBN_UP(1),
	TPS_GRAB(8),
	TPS_INVERT(16),
	TPS_MARGIN(4),
	TPS_PROXIMITY(1),
	TPS_QUEUE_ERR(2),
	TU_CENTIMETERS(2),
	TU_CIRCLE(3),
	TU_INCHES(1),
	TU_NONE(0),
	WTDC_CANCEL(1),
	WTDC_NONE(0),
	WTDC_OK(2),
	WTDC_RESTART(3),
	WTHC_ACTION(0),
	WTHC_GETLPLPFN(-3),
	WTHC_GETNEXT(1),
	WTHC_LPFNNEXT(-1),
	WTHC_LPLPFNNEXT(-2),
	WTHC_SKIP(2),
	WTH_PLAYBACK(1),
	WTH_RECORD(2),
	WTI_CURSORS(200),
	WTI_DDCTXS(400),
	WTI_DEFCONTEXT(3),
	WTI_DEFSYSCTX(4),
	WTI_DEVICES(100),
	WTI_DSCTXS(500),
	WTI_EXTENSIONS(300),
	WTI_INTERFACE(1),
	WTI_STATUS(2),
	WTP_DWDEFAULT(-1),
	WTP_LPDEFAULT(-1),
	WTX_CSRMASK(3),
	WTX_EXPKEYS(5),
	WTX_EXPKEYS2(8),
	WTX_FKEYS(1),
	WTX_OBT(0),
	WTX_TILT(2),
	WTX_TOUCHRING(7),
	WTX_TOUCHSTRIP(6),
	WTX_XBTNMASK(4),
	WT_DEFBASE(32752),
	WT_MAXOFFSET(15);
	
//	WT_PACKET    (WT_DEFBASE.long_ + 0),
//	WT_CTXOPEN   (WT_DEFBASE.long_ + 1),
//	WT_CTXCLOSE  (WT_DEFBASE.long_ + 2),
//	WT_CTXUPDATE (WT_DEFBASE.long_ + 3),
//	WT_CTXOVERLAP(WT_DEFBASE.long_ + 4),
//	WT_PROXIMITY (WT_DEFBASE.long_ + 5),
//	WT_INFOCHANGE(WT_DEFBASE.long_ + 6),
//	WT_CSRCHANGE (WT_DEFBASE.long_ + 7),
//	WT_PACKETEXT (WT_DEFBASE.long_ + 8),
//	WT_MAX       (WT_DEFBASE.long_ + WT_MAXOFFSET.long_);
	
	public final long long_;
	public final int int_;
	public final short short_;
	public final byte byte_;
	
	private WinTabDef(long value) {
		long_ = value;
		int_ = (int) value;
		short_ = (short) value;
		byte_ = (byte) value;
	}
	
	
	public static final int WT_PACKET     = (int) (32752 + 0);
	public static final int WT_CTXOPEN    = (int) (32752 + 1);
	public static final int WT_CTXCLOSE   = (int) (32752 + 2);
	public static final int WT_CTXUPDATE  = (int) (32752 + 3);
	public static final int WT_CTXOVERLAP = (int) (32752 + 4);
	public static final int WT_PROXIMITY  = (int) (32752 + 5);
	public static final int WT_INFOCHANGE = (int) (32752 + 6);
	public static final int WT_CSRCHANGE  = (int) (32752 + 7);
	public static final int WT_PACKETEXT  = (int) (32752 + 8);
	public static final int WT_MAX        = (int) (32752 + 15);
}


//public static final Memory buffer = new Memory(1048576); // 1 MiB buffer

//public static final int CRC_AGGREGATE = 2;
//public static final int CRC_INVERT = 4;
//public static final int CRC_MULTIMODE = 1;
//public static final int CSR_ACTIVE = 2;
//public static final int CSR_BTNNAMES = 6;
//public static final int CSR_BUTTONBITS = 5;
//public static final int CSR_BUTTONMAP = 7;
//public static final int CSR_BUTTONS = 4;
//public static final int CSR_CAPABILITIES = 19;
//public static final int CSR_MAX = 20;
//public static final int CSR_MINBUTTONS = 18;
//public static final int CSR_MINPKTDATA = 17;
//public static final int CSR_MODE = 16;
//public static final int CSR_NAME = 1;
//public static final int CSR_NPBTNMARKS = 10;
//public static final int CSR_NPBUTTON = 9;
//public static final int CSR_NPRESPONSE = 11;
//public static final int CSR_PHYSID = 15;
//public static final int CSR_PKTDATA = 3;
//public static final int CSR_SYSBTNMAP = 8;
//public static final int CSR_TPBTNMARKS = 13;
//public static final int CSR_TPBUTTON = 12;
//public static final int CSR_TPRESPONSE = 14;
//public static final int CSR_TYPE = 20;
//public static final int CTX_BTNDNMASK = 11;
//public static final int CTX_BTNUPMASK = 12;
//public static final int CTX_DEVICE = 6;
//public static final int CTX_INEXTX = 16;
//public static final int CTX_INEXTY = 17;
//public static final int CTX_INEXTZ = 18;
//public static final int CTX_INORGX = 13;
//public static final int CTX_INORGY = 14;
//public static final int CTX_INORGZ = 15;
//public static final int CTX_LOCKS = 4;
//public static final int CTX_MAX = 34;
//public static final int CTX_MOVEMASK = 10;
//public static final int CTX_MSGBASE = 5;
//public static final int CTX_NAME = 1;
//public static final int CTX_OPTIONS = 2;
//public static final int CTX_OUTEXTX = 22;
//public static final int CTX_OUTEXTY = 23;
//public static final int CTX_OUTEXTZ = 24;
//public static final int CTX_OUTORGX = 19;
//public static final int CTX_OUTORGY = 20;
//public static final int CTX_OUTORGZ = 21;
//public static final int CTX_PKTDATA = 8;
//public static final int CTX_PKTMODE = 9;
//public static final int CTX_PKTRATE = 7;
//public static final int CTX_SENSX = 25;
//public static final int CTX_SENSY = 26;
//public static final int CTX_SENSZ = 27;
//public static final int CTX_STATUS = 3;
//public static final int CTX_SYSEXTX = 31;
//public static final int CTX_SYSEXTY = 32;
//public static final int CTX_SYSMODE = 28;
//public static final int CTX_SYSORGX = 29;
//public static final int CTX_SYSORGY = 30;
//public static final int CTX_SYSSENSX = 33;
//public static final int CTX_SYSSENSY = 34;
//public static final int CXL_INASPECT = 2;
//public static final int CXL_INSIZE = 1;
//public static final int CXL_MARGIN = 8;
//public static final int CXL_SENSITIVITY = 4;
//public static final int CXL_SYSOUT = 16;
//public static final int CXO_CSRMESSAGES = 8;
//public static final int CXO_MARGIN = 32768;
//public static final int CXO_MESSAGES = 4;
//public static final int CXO_MGNINSIDE = 16384;
//public static final int CXO_PEN = 2;
//public static final int CXO_SYSTEM = 1;
//public static final int CXS_DISABLED = 1;
//public static final int CXS_OBSCURED = 2;
//public static final int CXS_ONTOP = 4;
//public static final int DVC_CSRDATA = 8;
//public static final int DVC_FIRSTCSR = 4;
//public static final int DVC_HARDWARE = 2;
//public static final int DVC_MAX = 19;
//public static final int DVC_NAME = 1;
//public static final int DVC_NCSRTYPES = 3;
//public static final int DVC_NPRESSURE = 15;
//public static final int DVC_ORIENTATION = 17;
//public static final int DVC_PKTDATA = 6;
//public static final int DVC_PKTMODE = 7;
//public static final int DVC_PKTRATE = 5;
//public static final int DVC_PNPID = 19;
//public static final int DVC_ROTATION = 18;
//public static final int DVC_TPRESSURE = 16;
//public static final int DVC_X = 12;
//public static final int DVC_XMARGIN = 9;
//public static final int DVC_Y = 13;
//public static final int DVC_YMARGIN = 10;
//public static final int DVC_Z = 14;
//public static final int DVC_ZMARGIN = 11;
//public static final int EXT_AXES = 5;
//public static final int EXT_CURSORS = 9;
//public static final int EXT_DEFAULT = 6;
//public static final int EXT_DEFCONTEXT = 7;
//public static final int EXT_DEFSYSCTX = 8;
//public static final int EXT_DEVICES = 110;
//public static final int EXT_MASK = 3;
//public static final int EXT_MAX = 210;
//public static final int EXT_NAME = 1;
//public static final int EXT_SIZE = 4;
//public static final int EXT_TAG = 2;
//public static final int HWC_HARDPROX = 4;
//public static final int HWC_INTEGRATED = 1;
//public static final int HWC_PHYSID_CURSORS = 8;
//public static final int HWC_TOUCH = 2;
//public static final int IFC_CTXOPTIONS = 7;
//public static final int IFC_CTXSAVESIZE = 8;
//public static final int IFC_IMPLVERSION = 3;
//public static final int IFC_MAX = 10;
//public static final int IFC_NCONTEXTS = 6;
//public static final int IFC_NCURSORS = 5;
//public static final int IFC_NDEVICES = 4;
//public static final int IFC_NEXTENSIONS = 9;
//public static final int IFC_NMANAGERS = 10;
//public static final int IFC_SPECVERSION = 2;
//public static final int IFC_WINTABID = 1;
//public static final int LCNAMELEN = 40;
//public static final int LC_NAMELEN = 40;
//public static final int ORD_WTClose = 22;
//public static final int ORD_WTConfig = 60;
//public static final int ORD_WTDataGet = 81;
//public static final int ORD_WTDataPeek = 82;
//public static final int ORD_WTEnable = 40;
//public static final int ORD_WTExtGet = 63;
//public static final int ORD_WTExtSet = 64;
//public static final int ORD_WTGet = 61;
//public static final int ORD_WTInfo = 20;
//public static final int ORD_WTMgrClose = 101;
//public static final int ORD_WTMgrConfigReplace = 141;
//public static final int ORD_WTMgrConfigReplaceEx = 202;
//public static final int ORD_WTMgrContextEnum = 120;
//public static final int ORD_WTMgrContextOwner = 121;
//public static final int ORD_WTMgrCsrButtonMap = 182;
//public static final int ORD_WTMgrCsrEnable = 181;
//public static final int ORD_WTMgrCsrExt = 185;
//public static final int ORD_WTMgrCsrPressureBtnMarks = 183;
//public static final int ORD_WTMgrCsrPressureBtnMarksEx = 201;
//public static final int ORD_WTMgrCsrPressureResponse = 184;
//public static final int ORD_WTMgrDefContext = 122;
//public static final int ORD_WTMgrDefContextEx = 206;
//public static final int ORD_WTMgrDeviceConfig = 140;
//public static final int ORD_WTMgrExt = 180;
//public static final int ORD_WTMgrOpen = 100;
//public static final int ORD_WTMgrPacketHook = 160;
//public static final int ORD_WTMgrPacketHookDefProc = 161;
//public static final int ORD_WTMgrPacketHookEx = 203;
//public static final int ORD_WTMgrPacketHookNext = 205;
//public static final int ORD_WTMgrPacketUnhook = 204;
//public static final int ORD_WTOpen = 21;
//public static final int ORD_WTOverlap = 41;
//public static final int ORD_WTPacket = 24;
//public static final int ORD_WTPacketsGet = 23;
//public static final int ORD_WTPacketsPeek = 80;
//public static final int ORD_WTQueuePackets = 83;
//public static final int ORD_WTQueuePacketsEx = 200;
//public static final int ORD_WTQueueSizeGet = 84;
//public static final int ORD_WTQueueSizeSet = 85;
//public static final int ORD_WTRestore = 66;
//public static final int ORD_WTSave = 65;
//public static final int ORD_WTSet = 62;
//public static final int PKEXT_ABSOLUTE = 1;
//public static final int PKEXT_RELATIVE = 2;
//public static final int PK_BUTTONS = 64;
//public static final int PK_CHANGED = 8;
//public static final int PK_CONTEXT = 1;
//public static final int PK_CURSOR = 32;
//public static final int PK_NORMAL_PRESSURE = 1024;
//public static final int PK_ORIENTATION = 4096;
//public static final int PK_ROTATION = 8192;
//public static final int PK_SERIAL_NUMBER = 16;
//public static final int PK_STATUS = 2;
//public static final int PK_TANGENT_PRESSURE = 2048;
//public static final int PK_TIME = 4;
//public static final int PK_X = 128;
//public static final int PK_Y = 256;
//public static final int PK_Z = 512;
//public static final int SBN_LCLICK = 1;
//public static final int SBN_LDBLCLICK = 2;
//public static final int SBN_LDRAG = 3;
//public static final int SBN_MCLICK = 7;
//public static final int SBN_MDBLCLICK = 8;
//public static final int SBN_MDRAG = 9;
//public static final int SBN_NONE = 0;
//public static final int SBN_P1CLICK = 112;
//public static final int SBN_P1DBLCLICK = 128;
//public static final int SBN_P1DRAG = 144;
//public static final int SBN_P2CLICK = 160;
//public static final int SBN_P2DBLCLICK = 176;
//public static final int SBN_P2DRAG = 192;
//public static final int SBN_P3CLICK = 208;
//public static final int SBN_P3DBLCLICK = 224;
//public static final int SBN_P3DRAG = 240;
//public static final int SBN_PNCLICK = 64;
//public static final int SBN_PNDBLCLICK = 80;
//public static final int SBN_PNDRAG = 96;
//public static final int SBN_PTCLICK = 16;
//public static final int SBN_PTDBLCLICK = 32;
//public static final int SBN_PTDRAG = 48;
//public static final int SBN_RCLICK = 4;
//public static final int SBN_RDBLCLICK = 5;
//public static final int SBN_RDRAG = 6;
//public static final int STA_BUTTONUSE = 7;
//public static final int STA_CONTEXTS = 1;
//public static final int STA_MANAGERS = 5;
//public static final int STA_MAX = 8;
//public static final int STA_PKTDATA = 4;
//public static final int STA_PKTRATE = 3;
//public static final int STA_SYSBTNUSE = 8;
//public static final int STA_SYSCTXS = 2;
//public static final int STA_SYSTEM = 6;
//public static final int TABLET_LOC_BOTTOM = 3;
//public static final int TABLET_LOC_LEFT = 0;
//public static final int TABLET_LOC_RIGHT = 1;
//public static final int TABLET_LOC_TOP = 2;
//public static final int TABLET_LOC_TRANSDUCER = 4;
//public static final int TABLET_PROPERTY_AVAILABLE = 2;
//public static final int TABLET_PROPERTY_CONTROLCOUNT = 0;
//public static final int TABLET_PROPERTY_FUNCCOUNT = 1;
//public static final int TABLET_PROPERTY_LOCATION = 11;
//public static final int TABLET_PROPERTY_MAX = 4;
//public static final int TABLET_PROPERTY_MIN = 3;
//public static final int TABLET_PROPERTY_OVERRIDE = 5;
//public static final int TABLET_PROPERTY_OVERRIDE_NAME = 6;
//public static final int TBN_DOWN = 2;
//public static final int TBN_NONE = 0;
//public static final int TBN_UP = 1;
//public static final int TPS_GRAB = 8;
//public static final int TPS_INVERT = 16;
//public static final int TPS_MARGIN = 4;
//public static final int TPS_PROXIMITY = 1;
//public static final int TPS_QUEUE_ERR = 2;
//public static final int TU_CENTIMETERS = 2;
//public static final int TU_CIRCLE = 3;
//public static final int TU_INCHES = 1;
//public static final int TU_NONE = 0;
//public static final int WTDC_CANCEL = 1;
//public static final int WTDC_NONE = 0;
//public static final int WTDC_OK = 2;
//public static final int WTDC_RESTART = 3;
//public static final int WTHC_ACTION = 0;
//public static final int WTHC_GETLPLPFN = (-3);
//public static final int WTHC_GETNEXT = 1;
//public static final int WTHC_LPFNNEXT = (-1);
//public static final int WTHC_LPLPFNNEXT = (-2);
//public static final int WTHC_SKIP = 2;
//public static final int WTH_PLAYBACK = 1;
//public static final int WTH_RECORD = 2;
//public static final int WTI_CURSORS = 200;
//public static final int WTI_DDCTXS = 400;
//public static final int WTI_DEFCONTEXT = 3;
//public static final int WTI_DEFSYSCTX = 4;
//public static final int WTI_DEVICES = 100;
//public static final int WTI_DSCTXS = 500;
//public static final int WTI_EXTENSIONS = 300;
//public static final int WTI_INTERFACE = 1;
//public static final int WTI_STATUS = 2;
//public static final int WTP_DWDEFAULT = -1;
//public static final int WTP_LPDEFAULT = -1;
//public static final int WTX_CSRMASK = 3;
//public static final int WTX_EXPKEYS = 5;
//public static final int WTX_EXPKEYS2 = 8;
//public static final int WTX_FKEYS = 1;
//public static final int WTX_OBT = 0;
//public static final int WTX_TILT = 2;
//public static final int WTX_TOUCHRING = 7;
//public static final int WTX_TOUCHSTRIP = 6;
//public static final int WTX_XBTNMASK = 4;
//public static final int WT_DEFBASE = 32752;
//public static final int WT_MAXOFFSET = 15;
