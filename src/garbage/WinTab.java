/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;


import com.sun.jna.Native;
import com.sun.jna.Pointer;


public class WinTab {
	
	static {
			Native.register("Wintab32.dll");
	}


	public final static int _WT_PACKET(int b) { return b+0; }
	public final static int _WT_CTXOPEN(int b) { return b+1; }
	public final static int _WT_CTXCLOSE(int b) { return b+2; }
	public final static int _WT_CTXUPDATE(int b) { return b+3; }
	public final static int _WT_CTXOVERLAP(int b) { return b+4; }
	public final static int _WT_PROXIMITY(int b) { return b+5; }
	public final static int _WT_INFOCHANGE(int b) { return b+6; }
	public final static int _WT_CSRCHANGE(int b) { return b+7; } /* 1.1 */
	public final static int _WT_PACKETEXT(int b) { return b+8; } /* 1.4 */
	public final static int _WT_MAX(int b) { return b+WinTabDef.WT_MAXOFFSET.int_; }
	
//	public final static native int WTInfoA(int wCategory, int nIndex, Pointer lpOutput);
//	public final static native Pointer WTOpenA(Pointer hWnd, WinTab_tagLOGCONTEXTA lpLogCtx, boolean fEnable);
//	public final static native boolean WTGetA(Pointer hCtx, WinTab_tagLOGCONTEXTA lpLogCtx);
//	public final static native boolean WTSetA(Pointer hCtx, WinTab_tagLOGCONTEXTA lpLogCtx);
	public final static native int WTInfoW(int wCategory, int nIndex, Pointer lpOutput);
	public final static native Pointer WTOpenW(Pointer hWnd, WT_structLOGCONTEXTW lpLogCtx, boolean fEnable);
	public final static native boolean WTClose(Pointer hCtx);
	public final static native int WTPacketsGet(Pointer hCtx, int cMaxPkts, Pointer lpPkts);
	public final static native boolean WTPacket(Pointer hCtx, int wSerial, Pointer lpPkt);
	public final static native boolean WTEnable(Pointer hCtx, boolean fEnable);
	public final static native boolean WTOverlap(Pointer hCtx, boolean fToTop);
	public final static native boolean WTConfig(Pointer hCtx, Pointer hWnd);
	public final static native boolean WTGetW(Pointer hCtx, WT_structLOGCONTEXTW lpLogCtx);
	public final static native boolean WTSetW(Pointer hCtx, WT_structLOGCONTEXTW lpLogCtx);
	public final static native boolean WTExtGet(Pointer hCtx, int wExt, Pointer lpData);
	public final static native boolean WTExtSet(Pointer hCtx, int wExt, Pointer lpData);
	public final static native boolean WTSave(Pointer hCtx, Pointer lpSaveInfo);
	public final static native Pointer WTRestore(Pointer hWnd, Pointer lpSaveInfo, boolean fEnable);
	public final static native int WTPacketsPeek(Pointer hCtx, int cMaxPkts, Pointer lpPkts);
	public final static native int WTDataGet(Pointer hCtx, int wBegin, int wEnd, int cMaxPkts, Pointer lpPkts, Pointer lpNPkts);
	public final static native int WTDataPeek(Pointer hCtx, int wBegin, int wEnd, int cMaxPkts, Pointer lpPkts, Pointer lpNPkts);
	public final static native boolean WTQueuePacketsEx(Pointer hCtx, Pointer lpOld, Pointer lpNew);
	public final static native int WTQueueSizeGet(Pointer hCtx);
	public final static native boolean WTQueueSizeSet(Pointer hCtx, int nPkts);
	
//	HMGR API WTMgrOpen(HWND, UINT);
//	BOOL API WTMgrClose(Pointer);
//	BOOL API WTMgrContextEnum(Pointer, WTENUMPROC, LPARAM);
//	HWND API WTMgrContextOwner(Pointer, HCTX);
//	HCTX API WTMgrDefContext(Pointer, BOOL);
//	HCTX API WTMgrDefContextEx(Pointer, UINT, BOOL); /* 1.1 */
//	UINT API WTMgrDeviceConfig(Pointer, UINT, HWND);
//	BOOL API WTMgrExt(Pointer, UINT, LPVOID);
//	BOOL API WTMgrCsrEnable(Pointer, UINT, BOOL);
//	BOOL API WTMgrCsrButtonMap(Pointer, UINT, LPBYTE, LPBYTE);
//	BOOL API WTMgrCsrPressureBtnMarks(Pointer, UINT, DWORD, DWORD);
//	BOOL API WTMgrCsrPressureResponse(Pointer, UINT, UINT FAR *, UINT FAR *);
//	BOOL API WTMgrCsrExt(Pointer, UINT, UINT, LPVOID);
//	BOOL API WTMgrConfigReplaceExA(Pointer, BOOL, LPSTR, LPSTR);
//	BOOL API WTMgrConfigReplaceExW(Pointer, BOOL, LPWSTR, LPSTR);
//	HWTHOOK API WTMgrPacketHookExA(Pointer, int, LPSTR, LPSTR);
//	HWTHOOK API WTMgrPacketHookExW(Pointer, int, LPWSTR, LPSTR);
//	BOOL API WTMgrPacketUnhook(HWTHOOK);
//	LRESULT API WTMgrPacketHookNext(HWTHOOK, int, WPARAM, LPARAM);
	
}
