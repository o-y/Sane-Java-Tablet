/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;

import java.util.ArrayList;

import net.screwbox.libs.sjt.TabletPacket;
import net.screwbox.libs.sjt.winimpl.wintab.structs.WT_structPACKET;

public class WinTabPacketThread extends Thread {
	
	private boolean stopRunning = false;
	private WindowsTabletImplementation windowsTabletImplementation;
	private ArrayList<TabletPacket> localPacketQueue;
	private WT_structPACKET wt_tagPACKET; 
	
	public WinTabPacketThread(
			WindowsTabletImplementation windowsTabletImplementation ) {
		this.windowsTabletImplementation = windowsTabletImplementation;
		localPacketQueue = new ArrayList<TabletPacket>();
		wt_tagPACKET = new WT_structPACKET();
	}

	
	public void requestShutdown() {
		stopRunning = true;
	}
	
	
	@Override
	public void run() {
		while (!stopRunning) {
			try {
				Thread.sleep(300);
				loopFunc();
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			loopFunc();
//			Thread.yield();
		}
	}
	

	private void loopFunc() {
////		if (doneTimes >= 3) return;
//		final int numPackets = WinTab.WTPacketsGet(
//				windowsTabletImplementation.wt_hCtx,
//				windowsTabletImplementation.queueSize,
//				windowsTabletImplementation.buffer
//		);
//		if (numPackets == 0) return;
////		+16(int) = serialnumber (sliderdata)
////		+16(int) = changed (packet)
////		for (int i=0; i<numPackets; i++) {
////			System.out.println(
////					windowsTabletImplementation.buffer.getInt(i*78+16)
////					);
////		}
//		System.out.println(
//				windowsTabletImplementation.buffer.getInt(16)
//				);
////		if (doneTimes == 2) {
////			byte[] puf = new byte[255];
////			windowsTabletImplementation.buffer.read(0, puf, 0, 255);
////			System.out.println(numPackets);
////			for (int i=0; i<255; i++) {
////				System.out.print(String.format(
////						"%02X ",
////						puf[i]
////				));
////				if (((1+i)%4) == 0) System.out.print(" ");
////			}
////			System.out.println("");
////		}
////		doneTimes++;
////		if(1==1)return;
////
////		// read all packets to local queue
////		for (int i=0; i<numPackets; i++) {
////			readPacket(i+1);
////		}
//		
//		// add all packets from local queue to concurrently accessible queue
////		queuePackets();
//		
//		WinTab.WTPacketsGet(windowsTabletImplementation.wt_hCtx, windowsTabletImplementation.queueSize, Pointer.NULL);
//	} // method
//
//
//	int doneTimes = 0;
//	private void readPacket(int packetIndex) {
//		if (doneTimes >= 10) return;
//		System.out.println(windowsTabletImplementation.buffer.getInt(32*packetIndex));
//		doneTimes++;
////		wt_tagPACKET.useMemory(windowsTabletImplementation.buffer, packetIndex);
////		wt_tagPACKET.read();
//		
////		System.out.println(wt_tagPACKET.pkX);
//	}
//
//
//	private void queuePackets() {
//		// TODO Auto-generated method stub
		
	}

	
} // class
