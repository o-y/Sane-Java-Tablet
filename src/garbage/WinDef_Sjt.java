/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;

import com.sun.jna.Pointer;

public class WinDef_Sjt {
	
	public static final int CW_USEDEFAULT = (int) 0x80000000L;
	public static final int WS_OVERLAPPED = (int) 0x00000000L;
	public static final int WS_CAPTION = (int) 0x00C00000L;
	public static final int WS_SYSMENU = (int) 0x00080000L;
	public static final int WS_THICKFRAME = (int) 0x00040000L;
	public static final int WS_MINIMIZEBOX = (int) 0x00020000L;
	public static final int WS_MAXIMIZEBOX = (int) 0x00010000L;
	public static final int WS_OVERLAPPEDWINDOW = 
		WS_OVERLAPPED |
		WS_CAPTION |
		WS_SYSMENU |
		WS_THICKFRAME |
		WS_MINIMIZEBOX |
		WS_MAXIMIZEBOX;
	public static final int WS_CLIPCHILDREN = (int) 0x02000000L;
	public static final int SW_SHOWNORMAL = (int) 1L;

//	public static final int GWLP_WNDPROC = (int) -4L;
//	public static final int WS_VISIBLE = (int) 0x10000000L;
	public static final int WM_CREATE = (int) 0x0001L;
	public static final int WM_DESTROY = (int) 0x0002L;
	public static final int WM_CLOSE = (int) 0x0010L;
	public static final int COLOR_APPWORKSPACE = (int) 12L;
	public static final int WM_ERASEBKGND = (int) 0x0014L;
	public static final int WM_PAINT = (int) 0x000FL;
	public static final int IDC_ARROW = (int) 32512L;
	public static final int WS_EX_TRANSPARENT  = (int)0x00000020L;
	public static final int WM_USER  = (int)0x0400L;
	public static final int WM_USER1  = (int)WM_USER+1;
	public static final int WM_USER2  = (int)WM_USER+2;
//	public static final int WM_USER3  = (int)WM_USER+3;

	
	public static final Pointer POINTER_ONE = new Pointer(1);
	public static final Pointer POINTER_ZERO = null;
	
} // class
