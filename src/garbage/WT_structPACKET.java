/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;

import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class WT_structPACKET extends Structure {
	//read 64
	public Pointer pkContext;
	public int pkStatus;
	public short pkTime;
	public int pkChanged;
	public int pkSerialNumber;
	public int pkCursor;
	public short pkButtons;
	public NativeLong pkX;
	public NativeLong pkY;
	public NativeLong pkZ;
	public int pkNormalPressure;
	public int pkTangentPressure;
	//struct = 52bytes this far down the fields
	
	public WT_structORIENTATION pkOrientation;
	//struct = 64bytes this far down the fields
	
	public WT_structROTATION pkRotation;
	//struct = 78bytes this far down the fields
	
	public int pkFKeys;
	//struct = 82bytes this far down the fields
	
	public WT_structTILT pkTilt;
	//struct = 90bytes this far down the fields
	
	
	public WT_structPACKET() {
	}
	
	
	public WT_structPACKET(Pointer p) {
		useMemory(p);
		read();
	}
	
	
	@Override
	public void useMemory(Pointer m, int offset) {
		super.useMemory(m, offset);
	}
	
}
