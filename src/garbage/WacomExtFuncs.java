/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;


import com.sun.jna.Native;
import com.sun.jna.Pointer;

import domain.libs.sjt.impl.TabletImplementationException;

public class WacomExtFuncs {
	
//	private final Memory buffer = new Memory(1048576); // 1 MiB buffer
//	private final Pointer bufferPtr = buffer.share(0);
	private WT_structEXTPROPERTY wt_structEXTPROPERTY = new WT_structEXTPROPERTY(); 
	
	
	// extGet
	
	private void extGet(
			Class<?> dataType,
			Pointer hCtx,
			int extensionTag,
			int tabletIndex,
			int controlIndex,
			int functionIndex,
			short propertyID) throws TabletImplementationException
	{
		wt_structEXTPROPERTY.version = 0;
		wt_structEXTPROPERTY.tabletIndex = (byte) tabletIndex;
		wt_structEXTPROPERTY.controlIndex = (byte) controlIndex;
		wt_structEXTPROPERTY.functionIndex = (byte) functionIndex;
		wt_structEXTPROPERTY.propertyID = propertyID;
		wt_structEXTPROPERTY.reserved = 0;
		wt_structEXTPROPERTY.dataSize = Native.getNativeSize(dataType);
		
		wt_structEXTPROPERTY.write();
		if (WinTab.WTExtGet(
				hCtx,
				extensionTag,
				wt_structEXTPROPERTY.getPointer()
				) == false) {
			throw new TabletImplementationException(
					"Failed to get tabletIndex(%d), controlIndex(%d), functionIndex(%d) as %s",
					tabletIndex,
					controlIndex,
					functionIndex,
					dataType.getSimpleName());
		}
		wt_structEXTPROPERTY.read();
		
//		buffer.write(0, wt_tagEXTPROPERTY.data, 0, Native.getNativeSize(dataType));
	} // method
	
	
	public int extGetInt(
			Pointer hCtx,
			int extensionTag,
			byte tabletIndex,
			byte controlIndex,
			byte functionIndex,
			short propertyID) throws TabletImplementationException
	{
		wt_structEXTPROPERTY.clear();
		wt_structEXTPROPERTY.read();
		extGet(
				int.class,
				hCtx,
				extensionTag,
				tabletIndex,
				controlIndex,
				functionIndex,
				propertyID);
		return wt_structEXTPROPERTY.data.getPointer().getInt(0);
	}
	
	
	public boolean extGetBool(
			Pointer hCtx,
			int extensionTag,
			byte tabletIndex,
			byte controlIndex,
			byte functionIndex,
			short propertyID) throws TabletImplementationException
	{
		wt_structEXTPROPERTY.clear();
		wt_structEXTPROPERTY.read();
		extGet(
				int.class,
				hCtx,
				extensionTag,
				tabletIndex,
				controlIndex,
				functionIndex,
				propertyID);
		return wt_structEXTPROPERTY.data.getPointer().getByte(0) != 0;
	}
	
	
	// extSet
	
	private void extSet(
			Class<?> dataType,
			Pointer hCtx,
			int extensionTag,
			byte tabletIndex,
			byte controlIndex,
			byte functionIndex,
			short propertyID) throws TabletImplementationException
	{
		wt_structEXTPROPERTY.read();
		wt_structEXTPROPERTY.version = 0;
		wt_structEXTPROPERTY.tabletIndex = tabletIndex;
		wt_structEXTPROPERTY.controlIndex = controlIndex;
		wt_structEXTPROPERTY.functionIndex = functionIndex;
		wt_structEXTPROPERTY.reserved = 0;
		wt_structEXTPROPERTY.dataSize = Native.getNativeSize(dataType);

		wt_structEXTPROPERTY.write();
		if (WinTab.WTExtSet(
				hCtx,
				extensionTag,
				wt_structEXTPROPERTY.getPointer()
				) == false) {
			throw new TabletImplementationException(
					"Failed to set extensionTag(%d), tabletIndex(%d), controlIndex(%d), functionIndex(%d) as %s",
					extensionTag,
					tabletIndex,
					controlIndex,
					functionIndex,
					dataType.getSimpleName());
		}
		wt_structEXTPROPERTY.read();
	} // method
	
	
	public void extSetInt(
			Pointer hCtx,
			int extensionTag,
			byte tabletIndex,
			byte controlIndex,
			byte functionIndex,
			short propertyID,
			int data) throws TabletImplementationException
	{
		wt_structEXTPROPERTY.clear();
		wt_structEXTPROPERTY.data.getPointer().setInt(0, data);
		
		extSet(int.class,
				hCtx,
				extensionTag,
				tabletIndex,
				controlIndex,
				functionIndex,
				propertyID
		);
	}
	
	
//	public void extSetBool(
//			Pointer hCtx,
//			int extensionTag,
//			byte tabletIndex,
//			byte controlIndex,
//			byte functionIndex,
//			short propertyID,
//			boolean data) throws TabletImplementationException
//	{
//		wt_tagEXTPROPERTY.data.getPointer().setInt(0, data==true?0:1);
//		extSet(int.class, hCtx, extensionTag, tabletIndex, controlIndex, functionIndex, propertyID);
//	}
	
	
	// other
	
//	public void setFunctionOverride(
//			HCTX hCtx,
//			BYTE tabletIndex,
//			UINT extensionTag,
//			BYTE controlIndex,
//			BYTE functionIndex,
//			boolean override) throws TabletImplementationException {
//		
//		// ask if control is available for override?
//		if (extGetBool(hCtx,
//				extensionTag,
//				tabletIndex,
//				controlIndex,
//				functionIndex,
//				WinTab.TABLET_PROPERTY_AVAILABLE))
//		{
//			System.out.println("was:"+extGetInt(hCtx, extensionTag, tabletIndex, controlIndex, functionIndex, WinTab.TABLET_PROPERTY_OVERRIDE));
//			extSetInt(hCtx,
//					extensionTag,
//					tabletIndex,
//					controlIndex,
//					functionIndex,
//					WinTab.TABLET_PROPERTY_OVERRIDE,
//					override==true?1:0);
//			System.out.println("is:"+extGetInt(hCtx, extensionTag, tabletIndex, controlIndex, functionIndex, WinTab.TABLET_PROPERTY_OVERRIDE));
//		}
//	}
//	
//	
//	public int getFunctionCount(
//			HCTX hCtx,
//			BYTE tabletIndex,
//			UINT extensionTag) throws TabletImplementationException {
//		return extGetInt(
//				hCtx,
//				extensionTag,
//				tabletIndex,
//				WinTab.ZEROBYTE,
//				WinTab.ZEROBYTE,
//				WinTab.TABLET_PROPERTY_FUNCCOUNT);
//	}
//	
//	
//	public int getControlCount(
//			HCTX hCtx,
//			BYTE tabletIndex,
//			UINT extensionTag) throws TabletImplementationException {
//		return extGetInt(
//				hCtx,
//				extensionTag,
//				tabletIndex,
//				WinTab.ZEROBYTE,
//				WinTab.ZEROBYTE,
//				WinTab.TABLET_PROPERTY_CONTROLCOUNT);
//	}
//	
//	
//	public Integer findExtensions(int tagToMatch) {
//		int foundTag;
//		for (int i=0; ; i++) {
//			if (WinTab.WTInfoW(
//					new UINT(WinTab.WTI_EXTENSIONS+i),
//					new UINT(WinTab.EXT_TAG), bufferPtr
//					) == 0) return null;
//			foundTag = buffer.getInt(0);
//			if (foundTag == tagToMatch) return foundTag;
//		}
//	}

} // class
