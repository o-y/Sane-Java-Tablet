/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;

import net.screwbox.libs.sanejavatablet.windows.wintab.WinTab;

import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class WinTab_tagLOGCONTEXT extends Structure {
	
	public byte lcName[] = new byte[WinTab.LCNAMELEN];
	public int lcOptions;
	public int lcStatus;
	public int lcLocks;
	public int lcMsgBase;
	public int lcDevice;
	public int lcPktRate;
	public int lcPktData;
	public int lcPktMode;
	public int lcMoveMask;
	public int lcBtnDnMask;
	public int lcBtnUpMask;
	public NativeLong lcInOrgX;
	public NativeLong lcInOrgY;
	public NativeLong lcInOrgZ;
	public NativeLong lcInExtX;
	public NativeLong lcInExtY;
	public NativeLong lcInExtZ;
	public NativeLong lcOutOrgX;
	public NativeLong lcOutOrgY;
	public NativeLong lcOutOrgZ;
	public NativeLong lcOutExtX;
	public NativeLong lcOutExtY;
	public NativeLong lcOutExtZ;
	public int lcSensX;
	public int lcSensY;
	public int lcSensZ;
	public boolean lcSysMode;
	public int lcSysOrgX;
	public int lcSysOrgY;
	public int lcSysExtX;
	public int lcSysExtY;
	public int lcSysSensX;
	public int lcSysSensY;
	
	
	public WinTab_tagLOGCONTEXT(Pointer p) {
		useMemory(p);
		read();
	}

}
