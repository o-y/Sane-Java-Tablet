/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class WT_structEXTPROPERTY extends Structure {
	
	public byte version;				// Structure version, 0 for now
	public byte tabletIndex;			// 0-based index for tablet
	public byte controlIndex;			// 0-based index for control 
	public byte functionIndex;			// 0-based index for control's sub-function
	public short propertyID;				// property ID
	public short reserved;				// DWORD-alignment filler
	public int dataSize;				// number of bytes in data[] buffer
	public DataPlaceHolder data;		// raw data
	public static class DataPlaceHolder extends Structure {
		public byte byteArray[] = new byte[256];
	}
	
		
	public WT_structEXTPROPERTY() {
	}
	
	
	public WT_structEXTPROPERTY(Pointer p) {
		useMemory(p);
		read();
	}

}
