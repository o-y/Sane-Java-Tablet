package domain.libs.sjt.garbage;

import com.sun.jna.IntegerType;

@SuppressWarnings("serial")
public class BYTE extends IntegerType {
	
	
	public BYTE() {
		super(1);
	}
	
	
	public BYTE(long value) {
		super(1, value);
	}
	
	
}
