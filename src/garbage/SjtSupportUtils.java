/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.misc;

import domain.libs.sjt.garbage.WinTabDef;

public class SjtSupportUtils {
	
	public static String bitFieldToString(long valueToCheck, WinTabDef... valuesToCheckFor) {
		StringBuilder sb = new StringBuilder();

		for (int i=0; i<valuesToCheckFor.length; i++) {
			if ((valueToCheck & valuesToCheckFor[i].long_) == valuesToCheckFor[i].long_) {
				sb.append(valuesToCheckFor[i].name());
				if (i != (valuesToCheckFor.length - 1)) sb.append(" | ");
				valueToCheck ^= valuesToCheckFor[i].long_;
			}
		}
		sb.insert(0,String.format(
				"%d (0x%X) = ", valueToCheck, valueToCheck
				));
		

		return sb.toString();
	}

	
	public static int getCharArrayStringLength(char[] charArray) {
		for (int i=0; i<charArray.length; i++) {
			if (charArray[i] == 0) return i;
		}
		return 0;
	}

	
}
