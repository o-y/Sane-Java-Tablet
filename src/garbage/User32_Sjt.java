/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;


import com.sun.jna.Native;
import com.sun.jna.Pointer;

public class User32_Sjt {
	
	static {
		Native.register("User32.dll");
	}
	
	
	public final static native Pointer CreateWindowExW(
			int dwExtStyle,
			Pointer lpClassName,
			Pointer lpWindowName,
			int dwStyle,
			int x,
			int y,
			int nWidth,
			int nHeight,
			Pointer hWndParent,
			Pointer hMenu,
			Pointer hInstance,
			Pointer lpParam);


	public final static native Pointer PostQuitMessage(int nExitCode);
	
	
	public final static native Pointer DefWindowProcW(
			Pointer hWnd,
			int Msg,
			Pointer wParam,
			Pointer lParam);
	
	
	public final static native short RegisterClassExW(Pointer lpWndClass);
	
	
	public final static native int UnregisterClassW(
			Pointer lpClassName,
			Pointer hInstance);
	
	
	public final static native int ShowWindow(
			Pointer hWnd,
			int nCmdShow);
	
	
	public final static native int DestroyWindow(
			Pointer hWnd);
	
	
	public final static native Pointer SendMessageW(
			Pointer hWnd,
			int Msg,
			Pointer wParam,
			Pointer lParam);
	
	
	public final static native int PostMessageW(
			Pointer hWnd,
			int Msg,
			Pointer wParam,
			Pointer lParam);
	
	
	public final static native int GetMessageW(
			Pointer lpMsg,
			Pointer hWnd,
			int wMsgFilterMin,
			int wMsgFilterMax);
	
	
	public final static native int TranslateMessage(Pointer lpMsg);
	
	
	public final static native Pointer DispatchMessageW(Pointer lpMsg);
	
	
	public final static native Pointer SetWindowLongPtrW(
			Pointer hWnd,
			int nIndex,
			Pointer dwNewLong);
	
	
	public final static native Pointer SetWindowLongPtrW(
			Pointer hWnd,
			int nIndex,
			WndProcCallback dwNewLong);
	

	public final static native Pointer GetWindowLongPtrW(
			Pointer hWnd,
			int nIndex);
	
	
	public final static native short UpdateWindow(Pointer hWnd);
	
	
	public final static native Pointer LoadCursorW(
			Pointer hInstance,
			Pointer lpCursorName);


}
