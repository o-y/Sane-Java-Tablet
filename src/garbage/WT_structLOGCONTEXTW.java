/* Copyright 2010 Fictive (Fictive's public key's fingerprint is "44:1a:41:70:b1:22:d4:93:3a:bb:84:62:60:0b:e4:a3")

This file is part of Sane Java Tablet.

Sane Java Tablet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sane Java Tablet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sane Java Tablet.  If not, see <http://www.gnu.org/licenses/>.
*/
package domain.libs.sjt.garbage;


import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import domain.libs.sjt.misc.SjtSupportUtils;

public class WT_structLOGCONTEXTW extends Structure {
	
	public char lcName[] = new char[WinTabDef.LCNAMELEN.int_];
	public int lcOptions;
	public int lcStatus;
	public int lcLocks;
	public int lcMsgBase;
	public int lcDevice;
	public int lcPktRate;
	public int lcPktData;
	public int lcPktMode;
	public int lcMoveMask;
	public int lcBtnDnMask;
	public int lcBtnUpMask;
	public NativeLong lcInOrgX;
	public NativeLong lcInOrgY;
	public NativeLong lcInOrgZ;
	public NativeLong lcInExtX;
	public NativeLong lcInExtY;
	public NativeLong lcInExtZ;
	public NativeLong lcOutOrgX;
	public NativeLong lcOutOrgY;
	public NativeLong lcOutOrgZ;
	public NativeLong lcOutExtX;
	public NativeLong lcOutExtY;
	public NativeLong lcOutExtZ;
	public int lcSensX;
	public int lcSensY;
	public int lcSensZ;
	public int lcSysMode;
	public int lcSysOrgX;
	public int lcSysOrgY;
	public int lcSysExtX;
	public int lcSysExtY;
	public int lcSysSensX;
	public int lcSysSensY;
	
	
	public WT_structLOGCONTEXTW(Pointer p) {
		useMemory(p);
		read();
	}
	
	
	public String toString() {
		return String.format(
				"lcName      = %s%n" +
				"lcOptions   = %s%n" +
				"lcStatus    = %s%n" +
				"lcLocks     = %s%n" +
				"lcMsgBase   = 0x%X (%d)%n" +
				"lcDevice    = %d%n" +
				"lcPktRate   = %d%n" +
				"lcPktData   = %s%n" +
				"lcPktMode   = %s%n" +
				"lcMoveMask  = %X (%d) %s%n" +
				"lcBtnDnMask = %X (%d) %s%n" +
				"lcBtnUpMask = %X (%d) %s%n" +
				"lcInOrgX    = %d%n" +
				"lcInOrgY    = %d%n" +
				"lcInOrgZ    = %d%n" +
				"lcInExtX    = %d%n" +
				"lcInExtY    = %d%n" +
				"lcInExtZ    = %d%n" +
				"lcOutOrgX   = %d%n" +
				"lcOutOrgY   = %d%n" +
				"lcOutOrgZ   = %d%n" +
				"lcOutExtX   = %d%n" +
				"lcOutExtY   = %d%n" +
				"lcOutExtZ   = %d%n" +
				"lcSensX     = %d%n" +
				"lcSensY     = %d%n" +
				"lcSensZ     = %d%n" +
				"lcSysMode   = %d%n" +
				"lcSysOrgX   = %d%n" +
				"lcSysOrgY   = %d%n" +
				"lcSysExtX   = %d%n" +
				"lcSysExtY   = %d%n" +
				"lcSysSensX  = %d%n" +
				"lcSysSensY  = %d",

				new String(lcName,0,SjtSupportUtils.getCharArrayStringLength(lcName)),
				format_lcOptions(),
				format_lcStatus(),
				format_lcLocks(),
				lcMsgBase, lcMsgBase,
				lcDevice,
				lcPktRate,
				format_lcPktData(lcPktData),
				format_lcPktData(lcPktMode),
				lcMoveMask, lcMoveMask, Integer.toBinaryString(lcMoveMask),
				lcBtnDnMask, lcBtnDnMask, Integer.toBinaryString(lcBtnDnMask),
				lcBtnUpMask, lcBtnUpMask, Integer.toBinaryString(lcBtnUpMask),
				lcInOrgX.longValue(),
				lcInOrgY.longValue(),
				lcInOrgZ.longValue(),
				lcInExtX.longValue(),
				lcInExtY.longValue(),
				lcInExtZ.longValue(),
				lcOutOrgX.longValue(),
				lcOutOrgY.longValue(),
				lcOutOrgZ.longValue(),
				lcOutExtX.longValue(),
				lcOutExtY.longValue(),
				lcOutExtZ.longValue(),
				lcSensX,
				lcSensY,
				lcSensZ,
				lcSysMode,
				lcSysOrgX,
				lcSysOrgY,
				lcSysExtX,
				lcSysExtY,
				lcSysSensX,
				lcSysSensY);
	}


	private String format_lcOptions() {
		return SjtSupportUtils.bitFieldToString(lcOptions,
				WinTabDef.CXO_SYSTEM,
				WinTabDef.CXO_PEN,
				WinTabDef.CXO_MESSAGES,
				WinTabDef.CXO_MARGIN,
				WinTabDef.CXO_MGNINSIDE,
				WinTabDef.CXO_CSRMESSAGES);
	}
	
	
	private Object format_lcStatus() {
		return SjtSupportUtils.bitFieldToString(lcStatus,
				WinTabDef.CXS_DISABLED,
				WinTabDef.CXS_OBSCURED,
				WinTabDef.CXS_ONTOP);
	}
	
	
	private Object format_lcLocks() {
		return SjtSupportUtils.bitFieldToString(lcLocks,
				WinTabDef.CXL_INSIZE,
				WinTabDef.CXL_INASPECT,
				WinTabDef.CXL_SENSITIVITY,
				WinTabDef.CXL_MARGIN,
				WinTabDef.CXL_SYSOUT);
	}


	private String format_lcPktData(int value) {
		return SjtSupportUtils.bitFieldToString(value,
				WinTabDef.PK_CONTEXT,
				WinTabDef.PK_STATUS,
				WinTabDef.PK_TIME,
				WinTabDef.PK_CHANGED,
				WinTabDef.PK_SERIAL_NUMBER,
				WinTabDef.PK_CURSOR,
				WinTabDef.PK_BUTTONS,
				WinTabDef.PK_X,
				WinTabDef.PK_Y,
				WinTabDef.PK_Z,
				WinTabDef.PK_NORMAL_PRESSURE,
				WinTabDef.PK_TANGENT_PRESSURE,
				WinTabDef.PK_ORIENTATION,
				WinTabDef.PK_ROTATION);
	}


}
