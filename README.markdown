#### Features that will be implemented
* Supports 32 and 64bit Windows and Linux.
* Supports multiple tablets.
* Supports basic tablet functionality (vendor-independent).
* Strives to support every Wacom tablet models' functionality.
* Supports grabbing of tablet to receive high resolution values from tablet. 
* For grabbing: Graphical tablet area mapping configuration tool.
* In any case: Graphical configuration tool where applicable.
* Can handle only one tool per tablet.
* Uses [Java Native Access](https://jna.dev.java.net/), thus all of the written code is pure Java.

#### Windows status
In progress.

#### Linux status
Has not been started yet.

#### OS X status
A possibility. Merge with another project?